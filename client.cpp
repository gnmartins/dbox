#include "client.h"

#include <iostream>
#include <sstream>
#include <cstring>

#include <unistd.h>
#include <pthread.h>
#include <pwd.h>

#include "ConnectionHandle.h"
#include "MessageQueue.h"
#include "SyncedDirectory.h"
#include "Packet.h"
#include "utils.h"

using std::cin;
using std::cout;
using std::endl;

/* ------------------------------------------------------------------------------------------------------------------ */

//! Message queue to send information to server
std::shared_ptr<MessageQueue> queue(nullptr);

//! Connection handle to the server
std::shared_ptr<ConnectionHandle> server_handle(nullptr);

//! Client's sync_dir
std::shared_ptr<SyncedDirectory> sync_dir(nullptr);

pthread_t thr_send;                 //! Thread to send information to server
pthread_t thr_recv;                 //! Thread to receive requests from server

pthread_t thr_sync;                 //! Thread to perform local synchronization

char username[MAXNAME];             //! Client's username
char path_to_syncdir[MAXNAME*2];    //! Path to client's sync_dir

/* ------------------------------------------------------------------------------------------------------------------ */

int main(int argc, char* argv[])
{
    /* --- command line parsing ----------------------------------------------------------- */
    if (argc < 4) {
        cout << "usage:" << endl;
        cout << "\t" << argv[0] << " username address port" << endl;
        exit(1);
    }

    char hostname[MAXNAME];
    uint16_t port;

    strcpy(username, argv[1]);
    strcpy(hostname, argv[2]);
    port = (uint16_t) atoi(argv[3]);

    /* --- starting connection ------------------------------------------------------------ */
    server_handle.reset(new ConnectionHandle(hostname, port, username));

    if (!server_handle->is_connected()) {
        exit(1);
    }

    Packet syn_ack = server_handle->recv_packet();
    if (syn_ack.get_type() == Packet::GBYE) {
        cout << "Connection refused by server." << endl;
        exit(1);
    }

    /* --- setting up sync_dir path ------------------------------------------------------- */
    const char* homedir;
    if ((homedir = getenv("HOME")) == nullptr) {
        homedir = getpwuid(getuid())->pw_dir;
    }
    sprintf(path_to_syncdir, "%s/sync_dir_%s", homedir, username);
    create_dir(path_to_syncdir);

    /* --- setting up sync directory ------------------------------------------------------ */
    queue.reset(new MessageQueue());
    sync_dir.reset(new SyncedDirectory(path_to_syncdir, queue));

    /* --- initial sync between client and server ----------------------------------------- */
    initial_sync();

    /* --- setting up thread to listen on socket ------------------------------------------ */
    pthread_create(&thr_recv, nullptr, handle_server_request, nullptr);

    /* --- setting up thread to upload files ---------------------------------------------- */
    pthread_create(&thr_send, nullptr, file_sender, nullptr);

    /* --- setting up thread to sync locally ---------------------------------------------- */
    pthread_create(&thr_sync, nullptr, local_sync, nullptr);

    /* --- waiting user input ------------------------------------------------------------- */
    handle_user_input();

    return 0;
}

void initial_sync()
{
    // request list of files
    server_handle->wait_send();
    server_handle->send_packet(Packet(Packet::LIST, 0, 0, 0));

    // receiving information on the "send" socket
    Packet packet = server_handle->recv_packet_swapped();
    std::shared_ptr<char> data = server_handle->recv_data_swapped(packet.get_total_size());
    server_handle->post_send();

    // dummy directory
    SyncedDirectory remote_directory;

    // setting received data for tokenizer
    std::string s_data(data.get(), packet.get_total_size());
    std::stringstream ss(s_data);

    // tokenize the received data into FileInfo and set to dummy directory
    // parse the received files and request/send updates as necessary
    std::string helper;
    while (getline(ss, helper, '|')) {
        FileInfo remote_file = FileInfo::deserialize(helper);
        remote_directory.set_file(remote_file);

        std::string filename = remote_file.get_filename();

        // if the file is on the local directory
        if (sync_dir->has_file(filename)) {

            FileInfo& local_file = sync_dir->get_file_info(filename);

            // if the local version is older...
            if (local_file.is_older(remote_file)) {

                // if the file has not been deleted, request to send
                if (remote_file.is_valid()) {
                    download_file(filename);
                }

                // otherwise, delete locally
                else {
                    sync_dir->set_file(remote_file);
                }

            }

            // if the local version is newer, send to server
            else if (remote_file.is_older(local_file)) {
                std::ostringstream oss;
                oss << sync_dir->get_path() << "/" << filename;
                queue->set_message(Message::MSG_UPDT_FILE, oss.str());
            }
        }

        // file does not exist locally, but exists on server
        else if (remote_file.is_valid()){
            download_file(filename);
        }
    }

    // send files from client that are not on the server
    std::string s2_data = sync_dir->serialize();
    std::stringstream ss2(s2_data);

    std::string helper2;
    while (getline(ss2, helper2, '|')) {
        FileInfo local_file = FileInfo::deserialize(helper2);
        std::string filename = local_file.get_filename();

        if (!remote_directory.has_file(filename)) {
            std::ostringstream oss;
            oss << sync_dir->get_path() << "/" << filename;
            queue->set_message(Message::MSG_UPDT_FILE, oss.str());
        }
    }
}

void* handle_server_request(void* args)
{
    while (true) {

        Packet x = server_handle->recv_packet();
        char* payload = x.get_payload();

        switch (x.get_type()) {

            case Packet::PUT: {
                FileInfo file_info = FileInfo::deserialize(payload, x.get_length());
                std::shared_ptr<char> data = server_handle->recv_data(file_info.get_size());
                file_info.set_data(data);

                if (sync_dir->set_file(file_info) == EXIT_SUCCESS) {

                    cout << "File `" << file_info.get_filename() << "` received from server ";
                    cout << "(path: `" << sync_dir->get_path() << "/" << file_info.get_filename();
                    cout << "`)." << endl;

                    fprintf(stderr, "\n$ ");
                }
                break;
            }

            case Packet::DEL: {
                FileInfo file_info = FileInfo::deserialize(payload, x.get_length());

                if (sync_dir->set_file(file_info) == EXIT_SUCCESS) {

                    cout << "File `" << file_info.get_filename() << "` deleted by server ";
                    cout << "(path: `" << sync_dir->get_path() << "/" << file_info.get_filename();
                    cout << "`." << endl;

                    fprintf(stderr, "\n$ ");
                }
                break;
            }

            case Packet::BCKP: {
                std::string new_port(payload, x.get_length());
                server_handle->update_send_addr((uint16_t) stoul(new_port));
                cout << "New server address: " << server_handle->get_peer_address() << endl;
                fprintf(stderr, "\n$ ");
                break;
            }

            default: {
                break;
            }

        }
    }

    return nullptr;
}

void* file_sender(void* args)
{
    while (true) {
        Message msg = queue->get_message();
        std::string filename = msg.get_content();

        switch (msg.get_type()) {
            case Message::MSG_UPDT_FILE: {
                upload_file(filename);
                fprintf(stderr, "\n$ ");
                break;
            }
            case Message::MSG_DELE_FILE: {
                delete_file(filename);
                fprintf(stderr, "\n$ ");
                break;
            }
            default: {
                break;
            }
        }
    }
}

void* local_sync(void* args)
{
    sync_dir->start_local_sync();
}

void handle_user_input()
{
    cout << endl << "$ ";

    std::string line, cmd, arg;

    while (getline(cin, line)) {
        std::istringstream iss(line);
        iss >> cmd;

        if (cmd == "download") {
            iss >> arg;

            if (arg.length() > 0) {
                download_file(arg);
            }
            else {
                cout << "Command syntax: `download <filename>`" << endl;
            }
        }

        else if (cmd == "upload") {
            iss >> arg;

            if (arg.length() > 0) {
                queue->set_message(Message::MSG_UPDT_FILE, arg);
                cout << "File `" << arg << "` added to upload queue." << endl;
            }
            else {
                cout << "Command syntax: `upload /path/to/file`" << endl;
            }
        }

        else if (cmd == "delete") {
            iss >> arg;

            if (arg.length() > 0) {
                queue->set_message(Message::MSG_DELE_FILE, arg);
                cout << "File `" << arg << "` added to deletion queue." << endl;
            }
            else {
                cout << "Command syntax: `delete <filename>`" << endl;
            }
        }

        else if (cmd == "list_client") {
            list_client();
        }

        else if (cmd == "list_server") {
            list_server();
        }

        else if (cmd == "exit") {
            close_connection();
            exit(EXIT_SUCCESS);
        }

        else {
            cout << "Unknown command." << endl;
        }

        cout << "$ ";
    }
}

void download_file(std::string& filename)
{
    server_handle->send_packet(Packet(Packet::GET, 0, 0, (uint16_t) filename.length(), filename.c_str()));

    cout << "File `" << filename << "` requested to server." << endl;
}

void upload_file(std::string& filename)
{
    FileInfo file_info = FileInfo::read_file(filename);
    std::string request = file_info.serialize_info();

    if (file_info.get_last_modified().length() == 0) {
        cout << "File `" << filename << "` does not exist." << endl;
        return;
    }

    // lock resource
    server_handle->wait_send();

    // send data
    server_handle->send_packet(Packet(Packet::PUT, 0, 0, (uint16_t) request.size(), request.c_str()));

    while (server_handle->send_data(file_info.get_data(), file_info.get_size()) != EXIT_SUCCESS) {
        cout << "Failed to send file `" << file_info.get_filename() << "`. Trying again..." << endl;
    }

    // wait for confirmation
    wait_for_confirmation();

    // unlock resource
    server_handle->post_send();

    cout << "File `" << filename << "` uploaded to server." << endl;
}

void delete_file(std::string& filename)
{
    std::ostringstream oss;
    oss << path_to_syncdir << "/" << filename;

    std::string path_to_file = oss.str();

    // reading directory again
    std::string path(path_to_syncdir);
    SyncedDirectory current_directory(path);

    if (current_directory.has_file(filename) || sync_dir->has_file(filename)) {
        FileInfo* old_f;
        if (sync_dir->has_file(filename)) {
            // setting file as deleted in current directory
            old_f = sync_dir->get_file_info_ptr(filename);
        }

        else {
            old_f = current_directory.get_file_info_ptr(filename);
        }

        std::shared_ptr<FileInfo> f(new FileInfo(old_f->get_name(),
                                                 old_f->get_extension(),
                                                 old_f->get_last_modified(),
                                                 old_f->get_size()));
        f->set_as_deleted();

        // changing last modified date
        std::stringstream ss;
        ss << time(NULL);
        f->set_last_modified(ss.str());

        // updating file in sync_dir
        sync_dir->set_file(*f);

        // notifying server
        std::string request = f->serialize_info();

        server_handle->wait_send();
        server_handle->send_packet(Packet(Packet::DEL, 0, 0, (uint16_t) request.length(), request.c_str()));
        wait_for_confirmation();
        server_handle->post_send();
    }

    else {
        cout << "File `" << filename << "` not found in `" << path_to_syncdir << "`." << endl;
        return;
    }

    cout << "File `" << filename << "` deleted from `" << path_to_syncdir << "`." << endl;
    cout << endl << "$ " << endl;
}

void list_client()
{
    std::string path(path_to_syncdir);
    SyncedDirectory directory(path);

    directory.print_directory();
}

void list_server()
{
    // request list of files
    server_handle->wait_send();
    server_handle->send_packet(Packet(Packet::LIST, 0, 0, 0));

    // receiving information on the "send" socket
    Packet packet = server_handle->recv_packet_swapped();
    std::shared_ptr<char> data = server_handle->recv_data_swapped(packet.get_total_size());
    server_handle->post_send();

    // dummy directory
    SyncedDirectory dir;

    // setting received data for tokenizer
    std::string s_data(data.get(), packet.get_total_size());
    std::stringstream ss(s_data);

    // tokenize the received data into FileInfo and set to dummy directory
    std::string helper;
    while (getline(ss, helper, '|')) {
        FileInfo z = FileInfo::deserialize(helper);
        dir.set_file(z);
    }

    dir.print_directory();
}

void close_connection()
{
    server_handle->wait_send();
    server_handle->send_packet(Packet(Packet::GBYE, 0, 0, 0));
    server_handle->post_send();
}

void wait_for_confirmation()
{
    bool confirmed = false;
    while (!confirmed) {
        Packet packet = server_handle->recv_packet_swapped();
        if (packet.get_type() == Packet::OK) {
            confirmed = true;
        }
    }
}