//
// Created by Gabriel on 28/05/2018.
//

#include <sstream>
#include "Message.h"

Message::Message(int type, std::string content)
        : type(type), content(content)
{

}

Message::Message(const Message& x)
{
    type = x.type;
    content = x.content;
}

Message::~Message()
{

}

int Message::get_type()
{
    return type;
}

std::string Message::get_content()
{
    return content;
}

std::string Message::pretty_print()
{
    std::ostringstream oss;
    oss << "MESSAGE\t type: ";

    switch (type) {
        case MSG_DELE_FILE:
            oss << "delete_file, ";
            break;

        case MSG_UPDT_FILE:
            oss << "update_file, ";
            break;

        default:
            oss << "unknown, ";
            break;
    }

    oss << "content: " << content;

    return oss.str();
}
