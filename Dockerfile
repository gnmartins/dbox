FROM ubuntu:14.04

RUN apt-get update && apt-get install -y \
	git \
	vim \
	make cmake \
	gcc g++	gdb