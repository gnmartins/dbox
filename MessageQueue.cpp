//
// Created by Gabriel on 28/05/2018.
//

#include "MessageQueue.h"

MessageQueue::MessageQueue()
{
    sem_init(&semaphore_consumer, 0, 0);
    sem_init(&changing_queue, 0, 1);
}

MessageQueue::~MessageQueue()
{

}

void MessageQueue::set_message(int type, std::string content)
{
    sem_wait(&changing_queue);

    messages.push(Message(type, content));
    sem_post(&semaphore_consumer);

    sem_post(&changing_queue);
}

Message MessageQueue::get_message()
{
    while (messages.size() == 0) {
        sem_wait(&semaphore_consumer);
    }

    sem_wait(&changing_queue);
    Message msg(messages.front());
    messages.pop();
    sem_post(&changing_queue);

    return msg;
}
