//
// Created by Gabriel on 05/06/2018.
//

#include "server.h"

#include <iostream>
#include <sstream>
#include <cstring>
#include <map>
#include <memory>

#include <unistd.h>
#include <pwd.h>

#include "ConnectionHandle.h"
#include "ClientInfo.h"
#include "utils.h"
#include "leader_election.h"

using std::cout;
using std::endl;

/* ------------------------------------------------------------------------------------------------------------------ */

//! Path to root folder of sync_dirs
char path_to_syncdirs[MAXNAME*2];

std::map<std::string, ClientInfo> clients;              //! Clients' metadata
std::map<std::string, SYNC_INFO> clients_sync_info;     //! Clients' synchronization data
sem_t sem_client_setup;                                 //! Semaphore to control client setup

/* ------------------------------------------------------------------------------------------------------------------ */

//! Connection handle to the master server
std::shared_ptr<ConnectionHandle> master_handle(nullptr);

int slave_id = -1;                          //! Replication server identification
std::vector<ConnectionHandle*> slaves;      //! List of connected replication servers
sem_t sem_slave_setup;                      //! Semaphore to control replication server setup

int elected = -1;                           //! Identification of newly elected master server
sem_t sem_election;                         //! Semaphore to control access to election functions

/* ------------------------------------------------------------------------------------------------------------------ */

int main(int argc, char* argv[])
{
    bool is_slave = false;
    char master_hostname[MAXNAME];
    uint16_t master_port = 0;

    bool selected_dir = false;
    char dir[MAXNAME];

    /* --- command line parsing ----------------------------------------------------------- */
    if (argc < 2) {
        cout << "usage:" << endl;
        cout << "\t" << argv[0] << " port";
        cout << "[--master address port] [--dir /path/to/server_directory]" << endl;
        exit(1);
    }

    uint16_t port = (uint16_t) atoi(argv[1]);

    if (argc >= 2) {
        for (int i = 2; i < argc; ++i) {
            if (strcmp(argv[i], "--master") == 0) {
                strcpy(master_hostname, argv[++i]);
                master_port = (uint16_t) atoi(argv[++i]);
                is_slave = true;
            }
            if (strcmp(argv[i], "--dir") == 0) {
                strcpy(dir, argv[++i]);
                selected_dir = true;
            }
        }
    }

    /* --- setting up main directory ------------------------------------------------------- */
    const char* homedir;
    if ((homedir = getenv("HOME")) == nullptr) {
        homedir = getpwuid(getuid())->pw_dir;
    }
    if (!selected_dir)
        sprintf(path_to_syncdirs, "%s/dbox-server", homedir);
    else
        sprintf(path_to_syncdirs, "%s", dir);

    create_dir(path_to_syncdirs);

    /* --- waiting for clients ------------------------------------------------------------- */
    sem_init(&sem_client_setup, 0, 1);
    sem_init(&sem_slave_setup, 0, 1);
    sem_init(&sem_election, 0, 1);

    /* --- SLAVE SERVER -------------------------------------------------------------------- */
    if (is_slave) {
        continue_as_slave(master_hostname, master_port);
        become_master();

        // slave will listen to a known port
        port = DEFAULT_FAILOVER_PORT;
    }

    /* --- MASTER SERVER ------------------------------------------------------------------- */
    cout << "Waiting for clients on port " << port << "." << endl;

    while (true) {
        ConnectionHandle* handle = new ConnectionHandle(port);
        std::string client_id = handle->get_peer_id();

        if (client_id == ".slave") {
            setup_new_slave(handle);
            continue;
        }

        setup_new_client(handle, client_id);
        handle->send_packet(Packet(Packet::HELO, 0, 0, 0));
    }
}

void setup_new_client(ConnectionHandle* handle, std::string& client_id)
{
    int device_id = 0;
    // saving client's information
    if (clients.count(client_id) == 0) {

        // creating directory
        char client_sync_dir[MAXNAME * 2];
        sprintf(client_sync_dir, "%s/%s", path_to_syncdirs, client_id.c_str());
        create_dir(client_sync_dir);

        // setting up client information
        clients[client_id] = ClientInfo(client_id, handle, client_sync_dir);
    }

    else {
        // client is already set, just setting new device
        device_id = clients[client_id].set_device(handle);

        if (device_id == -1) {
            // too many devices!
            handle->send_packet(Packet(Packet::GBYE, 0, 0, 0));
            cout << "Connection to client `" << client_id << "` refused ";
            cout << "-- too many connected devices." << endl;
            return;
        }
    }

    DEVICE dev;
    dev.user_id = client_id;
    dev.device_id = device_id;
    setup_device_threads(dev);

    // sync login to slaves
    if (slave_id == -1) {
        std::string address = handle->get_peer_address();
        update_client_on_slaves(address, client_id, true);
    }

    cout << "Client `" << client_id << "` connected ";
    cout << "(devices: " << clients[client_id].print_devices() << ")." << endl;
}

void setup_device_threads(DEVICE device)
{
    sem_wait(&sem_client_setup);

    if (clients_sync_info.count(device.user_id) == 0) {
        SYNC_INFO info;
        info.userid = device.user_id;
        clients_sync_info[device.user_id] = info;
        sem_init(&clients_sync_info[device.user_id].change_info, 0, 1);
    }

    sem_post(&sem_client_setup);

    SYNC_INFO& sync_info = clients_sync_info[device.user_id];

    DEVICE* arg = new DEVICE;
    arg->user_id = device.user_id;
    arg->device_id = device.device_id;

    DEVICE* arg2 = new DEVICE;
    arg2->user_id = device.user_id;
    arg2->device_id = device.device_id;

    sem_wait(&sync_info.change_info);
    sync_info.queue[device.device_id].reset(new MessageQueue());
    pthread_create(&sync_info.thr_send[device.device_id], nullptr, send_to_client, (void*) arg);
    pthread_create(&sync_info.thr_recv[device.device_id], nullptr, recv_from_client, (void*) arg2);
    sem_post(&sync_info.change_info);
}

void* send_to_client(void* _device)
{
    DEVICE dev = *(DEVICE*) _device;
    delete (DEVICE*) _device;

    std::string user = dev.user_id;
    int device = dev.device_id;

    ClientInfo& client_info = clients[user];
    ConnectionHandle* connection_handle = client_info.get_device_by_id(device);
    std::shared_ptr<MessageQueue> queue = clients_sync_info[user].queue[device];

    while (true) {

        Message msg = queue->get_message();
        std::string filename = msg.get_content();

        switch (msg.get_type()) {

            case Message::MSG_UPDT_FILE: {
                std::ostringstream filepath;
                filepath << client_info.get_directory_path() << "/" << filename;

                FileInfo file_info = FileInfo::read_file(filepath.str());
                std::string request = file_info.serialize_info();

                connection_handle->wait_send();
                connection_handle->send_packet(Packet(Packet::PUT, 0, 0, (uint16_t) request.size(), request.c_str()));
                connection_handle->send_data(file_info.get_data(), file_info.get_size());
                connection_handle->post_send();

                cout << "Sent file `" << filename << "` to `" << user << "` ";
                cout << "(device " << connection_handle->get_peer_address() << ")." << endl;
                break;
            }

            case Message::MSG_DELE_FILE: {
                FileInfo& file_info = client_info.get_file(filename);
                std::string request = file_info.serialize_info();

                connection_handle->wait_send();
                connection_handle->send_packet(Packet(Packet::DEL, 0, 0, (uint16_t) request.length(), request.c_str()));
                connection_handle->post_send();

                cout << "Notified `" << user << "` (device " << connection_handle->get_peer_address() << ") ";
                cout << "that file `" << filename << "` has been deleted." << endl;
                break;
            }

            default: {
                break;
            }

        }

    }

    return nullptr;
}

void* recv_from_client(void* _device)
{
    DEVICE dev = *(DEVICE*) _device;
    delete (DEVICE*) _device;

    std::string user = dev.user_id;
    int device = dev.device_id;

    ClientInfo& client_info = clients[user];
    SYNC_INFO& sync_info = clients_sync_info[user];
    ConnectionHandle* connection_handle = client_info.get_device_by_id(device);

    bool device_connected = true;
    while (device_connected) {
        Packet x = connection_handle->recv_packet();
        char* payload = x.get_payload();

        switch (x.get_type()) {

            case Packet::PUT: {
                FileInfo file_info = FileInfo::deserialize(payload, x.get_length());
                receive_file(dev, file_info);
                break;
            }

            case Packet::GET: {
                std::string filename(payload, x.get_length());
                sync_info.queue[device]->set_message(Message::MSG_UPDT_FILE, filename);
                break;
            }

            case Packet::DEL: {
                FileInfo file_info = FileInfo::deserialize(payload, x.get_length());
                delete_file(dev, file_info);
                break;
            }

            case Packet::LIST: {
                list_files(dev);
                break;
            }

            case Packet::GBYE: {
                close_connection(dev);
                device_connected = false;
                break;
            }

            default:
                break;

        }
    }

    return nullptr;
}

void receive_file(DEVICE dev, FileInfo& info)
{
    std::string user = dev.user_id;
    int device = dev.device_id;

    ClientInfo& client_info = clients[user];
    ConnectionHandle* connection_handle = client_info.get_device_by_id(device);

    // receive data on socket
    std::shared_ptr<char> file_data = connection_handle->recv_data(info.get_size());
    info.set_data(file_data);

    // sync file update to slaves
    update_file_on_slaves(info, user);

    // slaves received the message => send confirmation
    int confirmed;
    do {
        confirmed = connection_handle->send_packet_swapped(Packet(Packet::OK, 0, 0, 0));
    } while (confirmed != EXIT_SUCCESS);

    // saving file
    if (client_info.set_file(info) == EXIT_SUCCESS) {

        // sending information to next devices
        for (int i = 0; i < ClientInfo::MAX_DEVICES; ++i) {
            if (clients_sync_info[user].queue[i] != nullptr) {
                clients_sync_info[user].queue[i]->set_message(Message::MSG_UPDT_FILE, info.get_filename());
            }
        }

        cout << "Received file `" << info.get_filename() << "` from ";
        cout << "`" << user << "` (sent by " << connection_handle->get_peer_address() << ")." << endl;
    }
    else {
        cout << "Discarded older version of file `" << info.get_filename() << "` received from ";
        cout << "`" << user << "` (sent by " << connection_handle->get_peer_address() << ")." << endl;
    }
}

void delete_file(DEVICE dev, FileInfo& info)
{
    std::string user = dev.user_id;
    int device = dev.device_id;

    ClientInfo& client_info = clients[user];
    ConnectionHandle* connection_handle = client_info.get_device_by_id(device);

    // sync file update to slaves
    update_file_on_slaves(info, user);

    // slaves received the message => send confirmation
    int confirmed;
    do {
        confirmed = connection_handle->send_packet_swapped(Packet(Packet::OK, 0, 0, 0));
    } while (confirmed != EXIT_SUCCESS);

    // saving file
    std::string filename = info.get_filename();
    if (client_info.has_file(filename) && client_info.get_file(filename).is_valid()) {

        client_info.set_file(info);

        // sending information to next devices
        for (int i = 0; i < ClientInfo::MAX_DEVICES; ++i) {
            if (clients_sync_info[user].queue[i] != nullptr) {
                clients_sync_info[user].queue[i]->set_message(Message::MSG_DELE_FILE, info.get_filename());
            }
        }

        cout << "Deleted file `" << info.get_filename() << "` from ";
        cout << "`" << user << "` (requested by " << connection_handle->get_peer_address() << ")." << endl;
    }
    else {
        cout << "Ignoring request to delete file `" << info.get_filename() << "` from ";
        cout << "`" << user << "` (requested by " << connection_handle->get_peer_address() << ") ";
        cout << "-- file does not exist." << endl;
    }
}

void list_files(DEVICE dev)
{
    std::string user = dev.user_id;
    int device = dev.device_id;

    ClientInfo& client_info = clients[user];
    ConnectionHandle* connection_handle = client_info.get_device_by_id(device);

    // serializing directory entries from user
    std::string serialized_directory = client_info.get_serialized_directory();
    uint32_t length = (uint32_t) serialized_directory.length();

    // setting up data to be sent
    std::shared_ptr<char> data(new char[length]);
    memcpy(data.get(), serialized_directory.c_str(), length);

    // send info through the receiving socket (client will listen on its sending socket)
    connection_handle->send_packet_swapped(Packet(Packet::DATA, 0, length, 0));
    connection_handle->send_data_swapped(data, length);

    cout << "Sending list of files from `" << user << "` ";
    cout << "(requested by " << connection_handle->get_peer_address() << ")." << endl;
}

void close_connection(DEVICE dev)
{
    std::string user = dev.user_id;
    int device = dev.device_id;

    ClientInfo& client_info = clients[user];
    ConnectionHandle* connection_handle = client_info.get_device_by_id(device);

    sem_wait(&clients_sync_info[user].change_info);
    pthread_cancel(clients_sync_info[user].thr_send[device]);

    std::string address = connection_handle->get_peer_address();

    // sync logoff to slaves
    if (slave_id == -1) {
        update_client_on_slaves(address, user, false);
    }

    // cleanup
    client_info.remove_device(device);
    sem_post(&clients_sync_info[user].change_info);

    cout << "Client `" << user << "` ";
    cout << "(" << address << ") disconnected." << endl;
}

bool continue_as_slave(char* master_hostname, uint16_t master_port)
{
    bool connected_to_master;
    do {
        master_handle.reset(new ConnectionHandle(master_hostname, master_port, ".slave"));
        connected_to_master = master_handle->is_connected();
    } while(!connected_to_master);

    slave_id = -1;
    elected = -1;

    while (slave_id == -1) {
        Packet confirmation = master_handle->recv_packet();
        if (confirmation.get_type() == Packet::OK) {
            slave_id = confirmation.get_seqn();
        }
        else if (confirmation.get_type() == Packet::GBYE) {
            cout << "Connection refused by server." << endl;
            exit(1);
        }
    }

    cout << "Connected to master server " << master_handle->get_peer_address();
    cout << " (my id: " << slave_id << ")." << endl;

    pthread_t recv_thr;
    pthread_create(&recv_thr, nullptr, process_master_request, nullptr);

    master_handle->check_liveness();

    // master died!
    sem_wait(&sem_election);

    // election did not start yet...
    if (elected == -1) {
        pthread_cancel(recv_thr);

        int n_slaves = (int) slaves.size();
        ConnectionHandle* next = slaves[(slave_id+1)%n_slaves];

        elected = start_election(slave_id, slave_id, n_slaves, master_handle.get(), next);
    }

    sem_post(&sem_election);

    // this slave was elected as new master
    if (elected == slave_id) {
        cout << "I was elected as new master!" << endl;

        // cleanup
        for (int i = 0; i < slaves.size(); ++i) {
            if (slaves[i] != nullptr)
                delete slaves[i];
        }
        slaves.clear();
    }

    // another slave was elected as master
    else {
        ConnectionHandle* new_master = slaves[elected];
        std::string new_master_address = new_master->get_peer_address();

        cout << "Slave " << elected << " (" << new_master_address << ") ";
        cout << "is the new master!" << endl;

        // cleanup
        for (int i = 0; i < slaves.size(); ++i) {
            if (slaves[i] != nullptr)
                delete slaves[i];
        }
        slaves.clear();

        std::istringstream iss(new_master_address);
        std::string buffer;

        char new_hostname[MAXNAME];
        uint16_t new_port;

        std::getline(iss, buffer, ':');
        strcpy(new_hostname, buffer.c_str());

//        std::getline(iss, buffer, ':');
//        new_port = (uint16_t) stoul(buffer);
        new_port = DEFAULT_FAILOVER_PORT;

        return continue_as_slave(new_hostname, new_port);
    }

    return false;
}

void* process_master_request(void* arg)
{
    while (true) {

        Packet x = master_handle->recv_packet();

        switch (x.get_type()) {

            // new client
            case Packet::HELO: {
                std::string rcvd(x.get_payload(), x.get_length());
                std::stringstream ss(rcvd);

                std::string user;
                std::string address;
                std::string helper;
                bool is_connected;

                getline(ss, user, '|');
                getline(ss, address, '|');
                getline(ss, helper, '|');
                is_connected = (helper=="1");

                if (is_connected) {
                    cout << "User `" << user << "@" << address << "` logged in." << endl;

                    ConnectionHandle* handle = new ConnectionHandle();
                    handle->setup_sockets(user, address);
                    setup_new_client(handle, user);
                }
                else {
                    cout << "User `" << user << "@" << address << "` logged out." << endl;

                    int device;
                    for (device = 0; device < ClientInfo::MAX_DEVICES; ++device) {
                        ConnectionHandle* dev_ptr = clients[user].get_device_by_id(device);
                        if (dev_ptr && dev_ptr->get_peer_address() == address) {
                            break;
                        }
                    }
                    DEVICE dev;
                    dev.user_id = user;
                    dev.device_id = device;
                    close_connection(dev);
                }
                break;
            }

            // new slave
            case Packet::BCKP: {
                std::string s_name = ".slave";
                int new_slave_id = x.get_seqn();
                std::string address(x.get_payload(), x.get_length());

                if (slaves.size() < new_slave_id+1)
                    slaves.resize((unsigned long) new_slave_id+1);

                ConnectionHandle* slave_handle = new ConnectionHandle();
                slave_handle->setup_sockets(s_name, address);

                slaves[new_slave_id] = slave_handle;

                cout << "New slave #" << new_slave_id << ": " << slaves[new_slave_id]->get_peer_address() << endl;

                break;
            }

            // new file
            case Packet::PUT: {
                std::string user(x.get_payload(), x.get_length());
                std::shared_ptr<char> file_info_serialized = master_handle->recv_data(x.get_total_size());

                FileInfo file = FileInfo::deserialize(file_info_serialized.get(), x.get_total_size());
                clients[user].set_file(file);

                if (file.is_valid())
                    cout << "Updated file from `" << user << "`: ";
                else
                    cout << "Deleted file from `" << user << "`: ";
                cout << file.serialize_info() << endl;

                break;
            }

            // election is going on!
            case Packet::VOTE: {
                sem_wait(&sem_election);

                std::string ballot(x.get_payload(), x.get_length());

                int n_slaves = (int) slaves.size();
                ConnectionHandle* next = slaves[(slave_id+1)%n_slaves];

                elected = join_election(slave_id, ballot, master_handle.get(), next);
                sem_post(&sem_election);
                return nullptr;
            }

            default: {
                break;
            }
        }
    }

    return nullptr;
}

void setup_new_slave(ConnectionHandle* handle)
{
    sem_wait(&sem_slave_setup);

    int slave_id = (int) slaves.size();
    slaves.push_back(handle);

    handle->send_packet(Packet(Packet::OK, (uint16_t) slave_id, 0, 0));

    pthread_t thr;
    pthread_create(&thr, nullptr, slave_heartbeat, &slave_id);

    // inform new slave of connected slaves
    for (int id = 0; id < slaves.size(); ++id) {
        // get old slave address
        std::string slave_addr = slaves[id]->get_peer_address();
        // create packet
        Packet packet(Packet::BCKP, (uint16_t) id, 0, (uint16_t) slave_addr.size(), slave_addr.c_str());
        // send packet to new slave
        int ret;
        do {
            ret = handle->send_packet(packet);
        } while (ret != EXIT_SUCCESS);
    }

    sem_post(&sem_slave_setup);

    std::string address = handle->get_peer_address();
    update_slave_on_slaves(address, slave_id);

    cout << "Slave " << handle->get_peer_address() << " connected." << endl;

}

void* slave_heartbeat(void* _slave)
{
    int slave =  *(int*) _slave;

    sem_wait(&sem_slave_setup);
    ConnectionHandle* handle = slaves[slave];
    sem_post(&sem_slave_setup);

    while (true) {
        handle->recv_packet();
    }
}

void update_file_on_slaves(FileInfo& file, std::string& client)
{
    std::vector<pthread_t> wait_list;

    for (int i = 0; i < slaves.size(); ++i) {
        BCKP_FILE_UPDT* arg = new BCKP_FILE_UPDT(client, file);
        arg->slave = i;

        slave_update_file(arg);
//        pthread_t thr;
//        pthread_create(&thr, nullptr, slave_update_file, arg);
    }

    // TODO: join threads
}

void* slave_update_file(void* _args)
{
    BCKP_FILE_UPDT args = *(BCKP_FILE_UPDT*) _args;
    delete (BCKP_FILE_UPDT*) _args;

    std::string& client = args.client;
    FileInfo& file = args.file;
    int slave = args.slave;

    sem_wait(&sem_slave_setup);
    ConnectionHandle* handle = slaves[slave];
    sem_post(&sem_slave_setup);

    std::string request = file.serialize();
    Packet z(Packet::PUT, 0, (uint32_t) request.size(), (uint16_t) client.size(), client.c_str());

    handle->send_packet(z);

    std::shared_ptr<char> data(new char[request.size()]);
    memcpy(data.get(), request.c_str(), request.size());
    handle->send_data(data, (uint32_t) request.size());

    cout <<  "Finished updating slave " << slave << " with file `";
    cout << file.get_filename() << "` from user `" << client << "`." << endl;

    return nullptr;
}

void update_client_on_slaves(std::string& address, std::string& client, bool connected)
{
    std::vector<pthread_t> wait_list;

    for (int i = 0; i < slaves.size(); ++i) {
        BCKP_CLIENT_UPDT* arg = new BCKP_CLIENT_UPDT(client, address, connected);
        arg->slave = i;

        slave_update_client(arg);
//        pthread_t thr;
//        pthread_create(&thr, nullptr, slave_update_client, arg);
    }

    // TODO: join threads
}

void* slave_update_client(void* _args)
{
    BCKP_CLIENT_UPDT args = *(BCKP_CLIENT_UPDT*) _args;
    delete (BCKP_CLIENT_UPDT*) _args;

    std::string& client = args.client;
    std::string& address = args.address;
    bool is_connected = args.is_connected;
    int slave = args.slave;

    sem_wait(&sem_slave_setup);
    ConnectionHandle* handle = slaves[slave];
    sem_post(&sem_slave_setup);

    std::ostringstream oss;
    oss << client << "|" << address << "|" << is_connected;

    std::string request = oss.str();
    Packet z(Packet::HELO, 0, 0, (uint16_t) request.size(), request.c_str());

    handle->send_packet(z);

    cout <<  "Finished updating slave " << slave << " with new user ";
    cout << "(" << request << ")." << endl;

    return nullptr;
}

void update_slave_on_slaves(std::string& address, int id)
{
    std::vector<pthread_t> wait_list;

    for (int i = 0; i < slaves.size(); ++i) {
        BCKP_SLAVE_UPDT* arg = new BCKP_SLAVE_UPDT(address, id);
        arg->slave = i;

        if (i != id)
            slave_update_slave(arg);
//        pthread_t thr;
//        pthread_create(&thr, nullptr, slave_update_client, arg);
    }

    // TODO: join threads
}

void* slave_update_slave(void* _args)
{
    BCKP_SLAVE_UPDT args = *(BCKP_SLAVE_UPDT*) _args;
    delete (BCKP_SLAVE_UPDT*) _args;

    std::string& address = args.address;
    int new_slave = args.new_slave;
    int slave = args.slave;

    sem_wait(&sem_slave_setup);
    ConnectionHandle* handle = slaves[slave];
    sem_post(&sem_slave_setup);

    Packet z(Packet::BCKP, (uint16_t) new_slave, 0, (uint16_t) address.size(), address.c_str());

    handle->send_packet(z);

    cout <<  "Finished updating slave " << slave << " with new slave ";
    cout << "(" << address << ")." << endl;

    return nullptr;
}

void become_master()
{
    // iterate through connected clients
    for (auto& client : clients) {

        std::string client_id = client.first;
        ClientInfo& client_info = client.second;

        // iterate through devices
        for (int device = 0; device < ClientInfo::MAX_DEVICES; ++device) {

            ConnectionHandle* connection_handle = client_info.get_device_by_id(device);
            if (connection_handle == nullptr) continue;

            std::string my_address = connection_handle->get_my_address();
            std::string my_port = my_address.substr(8); // 0.0.0.0:<port> => port starts at pos 8

            Packet new_master_packet(Packet::BCKP, 0, 0, (uint16_t) my_port.length(), my_port.c_str());

            cout << "Informing " << client_id << "@" << connection_handle->get_peer_address();
            cout << " of new server..." << endl;

            int ret;
            do {
                ret = connection_handle->send_packet(new_master_packet);
            } while (ret != EXIT_SUCCESS);

            cout << "Client informed successfully!" << endl;

        }

    }
}