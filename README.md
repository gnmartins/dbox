# dbox

HOW TO RUN:
```bash
sudo apt-get install cmake  #necessary to compile
mkdir build                 #where build files will be put
cd build
cmake ..                    #generating Makefile
make                        #compiling

# now client and server can be run
./server 4242
./server 4243 --master localhost 4242 --dir slave1
./server 4243 --master localhost 4242 --dir slave2
./client user localhost 4242
```

Known Issues:
* Deleting a file and upload it again without changing anything does not work
