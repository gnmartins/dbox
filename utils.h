//
// Created by Gabriel on 07/06/2018.
//

#ifndef DBOX_UTILS_H
#define DBOX_UTILS_H

#include <string>
#include <vector>

#define MAXNAME 256

/**
 * Creates a directory in determined path
 *
 * @param dir Full path to create directory `/path/to/dir`
 */
void create_dir(char* dir);

/**
 * Creates a string indicating the contents of a boolean vector
 * { true, true, false, true } => 1,1,0,1
 *
 * @param v The vector of booleans
 * @return A string representation of the vector
 */
std::string serialize_bool_vector(std::vector<bool>& v);

/**
 * Fills a boolean vector with the contents of its string representation
 *
 * @param v Vector to be filled
 * @param s String representation of the boolean vector
 */
void deserialize_bool_vector(std::vector<bool>& v, std::string& s);

#endif //DBOX_UTILS_H
