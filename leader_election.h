//
// Created by Gabriel on 18/06/2018.
//

#ifndef DBOX_LEADER_ELECTION_H
#define DBOX_LEADER_ELECTION_H

#include <vector>
#include "ConnectionHandle.h"

/**
 * Starts a new master election between replication servers.
 * The election algorithm is the ring algorithm (Chang & Roberts, 1979).
 *
 * @param my_id Replication server identification
 * @param vote Vote being cast by the replication server
 * @param n Number of replication servers participating in the election
 * @param recv Connection handle to receive votes
 * @param send Connection handle to send votes
 * @return Returns the identification of the newly elected master server
 */
int start_election(int my_id, int vote, int n, ConnectionHandle* recv, ConnectionHandle* send);

/**
 * Joins an ongoing master election between replication servers.
 * The election algorithm is the ring algorithm (Chang & Roberts, 1979).
 *
 * @param my_id Replication server identification
 * @param ballot Voting ballot received from another replication server
 * @param recv Connection handle to receive votes
 * @param send Connection handle to send votes
 * @return Returns the identification of the newly elected master server
 */
int join_election(int my_id, std::string ballot, ConnectionHandle* recv, ConnectionHandle* send);

/**
 * Processes messages during the execution of a new master election.
 * The election algorithm is the ring algorithm (Chang & Roberts, 1979).

 * @param my_id Replication server identification
 * @param recv Connection handle to receive votes
 * @param send Connection handle to send votes
 * @param participants Vector containing the information of which replication servers are participating
 * @return Returns the identification of the newly elected master server
 */
int election(int my_id, ConnectionHandle* recv, ConnectionHandle* send, std::vector<bool>& participants);

/**
 * Creates a voting ballot based on the participants and the casted vote
 *
 * @param participants Boolean vector representing the participants
 * @param vote Casted vote
 * @return A string representation of a voting ballot
 */
std::string make_ballot(std::vector<bool>& participants, int vote);

/**
 * Parses a voting ballot string.
 *
 * @param ballot Voting ballot
 * @param participants Vector to place the information about participants
 * @return The vote casted on the ballot
 */
int parse_ballot(std::string& ballot, std::vector<bool>& participants);

#endif //DBOX_LEADER_ELECTION_H
