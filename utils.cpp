//
// Created by Gabriel on 07/06/2018.
//

#include "utils.h"

#include <sstream>

#include <sys/stat.h>

void create_dir(char* dir)
{
    struct stat st = {0};
    if (stat(dir, &st) == -1) {
        mkdir(dir, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    }
}

std::string serialize_bool_vector(std::vector<bool>& v)
{
    if (v.size() == 0) return std::string();

    std::ostringstream oss;
    oss << v[0];

    for (int i = 1; i < v.size(); ++i) {
        oss << "," << v[i];
    }

    return oss.str();
}

void deserialize_bool_vector(std::vector<bool>& v, std::string& s)
{
    std::istringstream iss(s);
    std::string buf;

    while (std::getline(iss, buf, ',')) {
        v.push_back(buf=="1");
    }
}