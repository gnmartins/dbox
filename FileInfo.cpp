//
// Created by Gabriel on 24/05/2018.
//

#include "FileInfo.h"

#include <cstring>
#include <fstream>
#include <sstream>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <iostream>


FileInfo::FileInfo(const std::string& name, const std::string& extension, const std::string& last_modified,
                   uint32_t size)
        : name(name), extension(extension), last_modified(last_modified), size(size)
{
    data.reset();
}

FileInfo::FileInfo(const std::string& name, const std::string& extension, const std::string& last_modified,
                   uint32_t size, const char* _data)
        : name(name), extension(extension), last_modified(last_modified), size(size)
{
    data.reset(new char[size]);
    memcpy(data.get(), _data, size);
}

FileInfo FileInfo::_read_file(const std::string& path, bool fetch_data)
{
    std::stringstream ss;

    std::string name;
    std::string ext;
    std::string lm;
    uint32_t size = 0;
    std::string data;

    // name
    name = path;
    std::size_t slash = path.find_last_of("/\\");
    if (slash != std::string::npos)
        name = path.substr(slash+1);

    // extension
    std::size_t dot = name.find_last_of(".");
    if (dot != std::string::npos) {
        ext = name.substr(dot+1);
        name = name.substr(0, dot);
    }

    // size in bytes + last modified date
    struct stat st{};
    if (stat(path.c_str(), &st) == 0) {
        size = (uint32_t) st.st_size;

        ss << st.st_mtime;
        lm = ss.str();
    }

    // content
    if (fetch_data) {
        std::ifstream f(path);
        data = std::string((std::istreambuf_iterator<char>(f)),
                           std::istreambuf_iterator<char>());

        f.close();
        return FileInfo(name, ext, lm, size, data.c_str());
    }
    else {
        return FileInfo(name, ext, lm, size);
    }
}

FileInfo FileInfo::deserialize(std::string& serialized)
{
    std::istringstream iss(serialized);

    std::string hdr, tag, comma, buffer;
    std::string n, e, lm, d;
    uint32_t s;
    bool v;

    iss >> hdr >> tag;
    iss >> n;
    n.pop_back();

    iss >> tag;
    iss >> e;
    e.pop_back();

    iss >> tag;
    iss >> lm;
    lm.pop_back();

    iss >> tag;
    iss >> s;

    iss >> comma >> tag;
    iss >> v;

    iss >> comma >> tag;
    d = iss.str().substr(iss.tellg());

    FileInfo file = (d.size()-1 == s)?
            FileInfo(n, e, lm, s, d.c_str()+1) : FileInfo(n, e, lm, s);

    if (!v) file.set_as_deleted();

    return file;
}

FileInfo FileInfo::deserialize(char* serialized_char, uint32_t length)
{
    std::string serialized(serialized_char, length);
    return FileInfo::deserialize(serialized);
}

std::string FileInfo::_serialize(bool ignore_data)
{
    std::ostringstream oss;

    oss << "FILE ";
    oss << "name= " << name << ", ";
    oss << "extension= " << extension << ", ";
    oss << "last_modified= " << last_modified << ", ";
    oss << "size= " << size << ", ";
    oss << "valid= " << validity << ", ";

    if (data.get() != nullptr && !ignore_data) {
        oss << "data= " << std::string(data.get(), size);
    }
    else {
        oss << "data= ";
    }

    return oss.str();
}

std::string FileInfo::get_name()
{
    return name;
}

std::string FileInfo::get_extension()
{
    return extension;
}

std::string FileInfo::get_filename()
{
    std::string filename;

    if (extension.length() > 0) {
        filename = name + "." + extension;
    }
    else {
        filename = name;
    }

    return filename;
}

std::string FileInfo::get_last_modified()
{
    return last_modified;
}

uint32_t FileInfo::get_size()
{
    return size;
}

std::string FileInfo::get_last_modified_date()
{
    char d[MAXNAME];
    char* end;

    long t = strtol(last_modified.c_str(), &end, 10);
    struct tm* p = localtime(&t);

    strftime(d, MAXNAME, "%b %d %H:%M", p);

    return std::string(d);
}

std::shared_ptr<char> FileInfo::get_data()
{
    return data;
}

void FileInfo::set_last_modified(const std::string& new_lm)
{
    last_modified = new_lm;
}

void FileInfo::set_size(uint32_t new_size)
{
    size = new_size;
}

void FileInfo::set_data(std::shared_ptr<char> new_data)
{
    data = new_data;
}

void FileInfo::set_as_created()
{
    validity = true;
}

void FileInfo::set_as_deleted()
{
    validity = false;
}

bool FileInfo::is_older(FileInfo& other)
{
    return get_last_modified() < other.get_last_modified();
}

bool FileInfo::is_valid()
{
    return validity;
}

