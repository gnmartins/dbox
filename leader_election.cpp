//
// Created by Gabriel on 18/06/2018.
//

#include "leader_election.h"

#include <iostream>
#include <vector>
#include <sstream>

#include "utils.h"

int start_election(int my_id, int vote, int n, ConnectionHandle* recv, ConnectionHandle* send)
{
    std::cout << "Starting election..." << std::endl;

    std::vector<bool> participants(n, false);

    participants[my_id] = true;

    std::string ballot = make_ballot(participants, vote);

    // std::cout << "Sending vote: \"" << ballot << "\"." << std::endl;

    Packet vote_packet(Packet::VOTE, 0, 0, (uint16_t) ballot.size(), ballot.c_str());
    send->send_packet(vote_packet);

    // std::cout << "Sent vote to " << send->get_peer_address() << "." << std::endl;

    int elected = election(my_id, recv, send, participants);

    return elected;
}

int join_election(int my_id, std::string ballot, ConnectionHandle* recv, ConnectionHandle* send)
{
    std::cout << "Joining election..." << std::endl;

    std::vector<bool> participants;
    int rcvd_vote = parse_ballot(ballot, participants);

    if (rcvd_vote > my_id) {
        // std::cout << "Received higher id, forwarding..." << std::endl;

        participants[my_id] = true;
        ballot = make_ballot(participants, rcvd_vote);

        // std::cout << "Ballot: \"" << ballot << "\"." << std::endl;

        Packet new_packet(Packet::VOTE, 0, 0, (uint16_t) ballot.size(), ballot.c_str());
        send->send_packet(new_packet);
    }
    else {
        // std::cout << "Received lower id..." << std::endl;
        // std::cout << "I am not participating, casting my new vote..." << std::endl;

        // change and forward
        participants[my_id] = true;
        ballot = make_ballot(participants, my_id);

        // std::cout << "Ballot: \"" << ballot << "\"." << std::endl;

        Packet new_packet(Packet::VOTE, 0, 0, (uint16_t) ballot.size(), ballot.c_str());
        send->send_packet(new_packet);
    }

    return election(my_id, recv, send, participants);
}

int election(int my_id, ConnectionHandle* recv, ConnectionHandle* send, std::vector<bool>& participants)
{
    std::string ballot;
    int elected = -1;
    int rcvd_vote;
    int its = 0;
    do {
        // std::cout << "iteraction " << its++ << std::endl;
        Packet packet = recv->recv_packet();

        // received a voting ballot
        if (packet.get_type() == Packet::VOTE) {

            ballot = std::string(packet.get_payload(), packet.get_length());
            rcvd_vote = parse_ballot(ballot, participants);

            // received own id => end election
            if (rcvd_vote == my_id) {
                // std::cout << "I was elected!" << std::endl;

                Packet new_packet(Packet::HELO, my_id, 0, 0);
                send->send_packet(new_packet);
            }

            // received higher id => forward message
            else if (rcvd_vote > my_id) {
                // std::cout << "Received higher id, forwarding..." << std::endl;

                participants[my_id] = true;
                ballot = make_ballot(participants, rcvd_vote);

                // std::cout << "Ballot: " << ballot << std::endl;

                Packet new_packet(Packet::VOTE, 0, 0, (uint16_t) ballot.size(), ballot.c_str());
                send->send_packet(new_packet);
            }

            // received lower id => change and forward if not participating
            else {
                // std::cout << "Received lower id..." << std::endl;

                // I am participating => ignore
                if (participants[my_id]) continue;

                // std::cout << "I am not participating, casting my new vote..." << std::endl;

                // change and forward
                participants[my_id] = true;
                ballot = make_ballot(participants, my_id);

                // std::cout << "Ballot: " << ballot << std::endl;

                Packet new_packet(Packet::VOTE, 0, 0, (uint16_t) ballot.size(), ballot.c_str());
                send->send_packet(new_packet);
            }
        }

        // someone was elected
        else if (packet.get_type() == Packet::HELO) {
            elected = packet.get_seqn();

            // std::cout << elected << " was elected." << std::endl;

            if (elected != my_id) {
                send->send_packet(packet);
            }
        }

    } while (elected < 0);

    return elected;
}

std::string make_ballot(std::vector<bool>& participants, int vote)
{
    std::ostringstream oss;
    oss << vote << " " << serialize_bool_vector(participants);

    return oss.str();
}

int parse_ballot(std::string& ballot, std::vector<bool>& participants)
{
    std::istringstream iss(ballot);

    int vote;
    std::string serialized_participants;

    iss >> vote >> serialized_participants;

    deserialize_bool_vector(participants, serialized_participants);

    return vote;
}
