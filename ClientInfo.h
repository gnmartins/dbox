//
// Created by Gabriel on 10/06/2018.
//

#ifndef DBOX_CLIENTINFO_H
#define DBOX_CLIENTINFO_H

#include <cstdint>

#include "ConnectionHandle.h"
#include "SyncedDirectory.h"

/**
 * CLIENTINFO
 *
 *  Representation of client's information
 */
class ClientInfo {

public:
    /**
     * Default constructor for empty client
     */
    ClientInfo() = default;

    /**
     * Creates a client information with only the client's username
     * @param userid Client's username
     */
    ClientInfo(std::string& userid);

    /**
     * Creates a client information while reading its directory information
     *
     * @param userid Client's username
     * @param path Client's directory
     */
    ClientInfo(std::string& userid, char* path);

    /**
     * Creates a client information associating it with a device connection handle
     *
     * @param userid Client's username
     * @param device Client's connection handle
     */
    ClientInfo(std::string& userid, ConnectionHandle* device);

    /**
     * Creates a client information while reading its directory information and associating it
     * with a device connection handle
     *
     * @param userid Client's username
     * @param device Client's connection handle
     * @param path Client's directory
     */
    ClientInfo(std::string& userid, ConnectionHandle* device, char* path);

    /**
     * Listen for a new client.
     *
     * @param port Port to listen clients on.
     * @return The new client's information
     */
    static ClientInfo remote_client(uint16_t port);

    //! Maximum simultaneous devices for a client
    const static int MAX_DEVICES = 2;

    /**
     * Checks whether the client is logged in.
     * This iterates through the connected devices checking their connection status.
     *
     * @return Returns `true` if at least one device is connected, `false` otherwise
     */
    bool is_logged_in();

    /**
     * Returns a client's device connection handle
     *
     * @param device_id Device identification
     * @return The connection handle for the device
     */
    ConnectionHandle* get_device_by_id(int device_id);

    /**
     * Returns the serialized information of the client's directory
     *
     * @return A string representing the directory
     */
    std::string get_serialized_directory();

    /**
     * Sets a new device to the client
     *
     * @param device Connection handle of the device
     * @return EXIT_SUCCESS if the device was set, EXIT_FAILURE if the maximum number of devices has been reached
     */
    int set_device(ConnectionHandle* device);

    /**
     * Sets a file to the client's directory
     *
     * @param file File information
     * @return EXIT_SUCCESS if the file was set, EXIT_FAILURE otherwise
     */
    int set_file(FileInfo& file);

    /**
     * Returns the path to the client's directory
     *
     * @return A string containing /path/to/clients/sync_dir
     */
    std::string get_directory_path();

    /**
     * Returns the file information stored in the client's directory
     *
     * @param filename File's name
     * @return The client's file information
     */
    FileInfo& get_file(std::string& filename);

    /**
     * Checks whether a file is present in the directory
     *
     * @param filename File's name
     * @return Returns `true` if the file exists, `false` otherwise
     */
    bool has_file(std::string& filename);

    /**
     * Remove a client's device
     *
     * @param device Device identification
     */
    void remove_device(int device);

    /**
     * Returns a string containing all connected devices' addresses
     *
     * @return A string in format `<address>:<port>, <address>:<port>, [...]`
     */
    std::string print_devices();

private:
    std::string userid;
    ConnectionHandle* connected_devices[MAX_DEVICES] = {nullptr, nullptr};
    std::shared_ptr<SyncedDirectory> directory;
};


#endif //DBOX_CLIENTINFO_H
