//
// Created by Gabriel on 05/06/2018.
//

#ifndef DBOX_SERVER_H
#define DBOX_SERVER_H

#include <string>

#include "ClientInfo.h"

//! Default port used by a replication server after failover to accept new connections
#define DEFAULT_FAILOVER_PORT 4242;

/**
 * SYNC_INFO
 *
 *  Structure used to store a client's synchronization metadata
 */
typedef struct sync_info {
    //! Client's username
    std::string userid;

    //! Threads used to send information to connected devices
    pthread_t thr_send[ClientInfo::MAX_DEVICES];

    //! Threads used to receive information from connected devices
    pthread_t thr_recv[ClientInfo::MAX_DEVICES];

    //! Queues to store messages to be sent to devices
    std::shared_ptr<MessageQueue> queue[ClientInfo::MAX_DEVICES];

    //! Semaphore to control change of client's information
    sem_t change_info;
} SYNC_INFO;

/**
 * DEVICE
 *
 *  Representation of a client's device.
 *  Used mainly as argument to threads handling device specific input
 */
typedef struct device {
    //! Client's username
    std::string user_id;

    //! Device identification
    int device_id;
} DEVICE;

/**
 * BCKP_FILE_UPDT
 *
 *  Representation of a client's file updated.
 *  Used mainly as argument to threads handling server replication
 */
typedef struct bckp_file_updt {
    //! Replication server identification
    int slave;

    //! Client's username associated with the file
    std::string& client;

    //! File information
    FileInfo& file;

    /**
     * Struct constructor
     *
     * @param c Client's username
     * @param f File information
     */
    bckp_file_updt(std::string& c, FileInfo& f) : client(c), file(f) {}
} BCKP_FILE_UPDT;

/**
 * BCKP_CLIENT_UPDT
 *
 *  Representation of a client's login/logoff.
 *  Used mainly as argument to threads handling server replication
 */
typedef struct bckp_client_updt {
    //! Replication server identification
    int slave;

    //! Client's username associated with the file
    std::string& client;

    //! Client's IP address in format `<address>:<port>`
    std::string& address;

    //! Client login information (`true` if client is connected to the server)
    bool is_connected;

    /**
     * Struct constructor
     *
     * @param c Client's username
     * @param a Client's address
     * @param i Client's login information
     */
    bckp_client_updt(std::string& c, std::string& a, bool i) : client(c), address(a), is_connected(i) {}
} BCKP_CLIENT_UPDT;

/**
 * BCKP_SLAVE_UPDT
 *
 *  Representation of the connection of a new replication server
 *  Used mainly as argument to threads handling server replication.
 */
typedef struct bckp_slave_updt {
    //! Replication server identification
    int slave;

    //! New replication server address
    std::string& address;

    //! New replication server identification
    int new_slave;

    /**
     * Struct constructor
     *
     * @param a New replication server address
     * @param i New replication server identification
     */
    bckp_slave_updt(std::string& a, int i) : address(a), new_slave(i) {}
} BCKP_SLAVE_UPDT;

/**
 * Setups a new client connecting to the server.
 * This function creates a logical/physical directory associated to the client, and
 * sets up the necessary metadata, such as the synchronization structures.
 * This function also informs the replication servers of the newly connected client.
 *
 * @param handle Connection handle for the new client's device
 * @param client_id Client's username
 */
void setup_new_client(ConnectionHandle* handle, std::string& client_id);

/**
 * Setups the send and receive threads to the client's device.
 *
 * @param device Client's new device information
 */
void setup_device_threads(DEVICE device);

/**
 * Receives and handles requests from client.
 *
 * @param _device Client's device information
 * @return Returns a nullptr
 */
void* recv_from_client(void* _device);

/**
 * Sends file updates to the client based on its message queue.
 *
 * @param _device Client's device information
 * @return Returns a nullptr
 */
void* send_to_client(void* _device);

/**
 * Receives a file from a specific client's device.
 * This functions saves the file on the logical and physical directory whenever necessary.
 * The file information is also replicated to the replication servers and other client devices.
 *
 * @param dev Client's device information which performed the request
 * @param info Initial file information that will be received
 */
void receive_file(DEVICE dev, FileInfo& info);

/**
 * Deletes a file from a specific client's directory.
 * This function also replicates the deletion to the replication server and other client devices.
 *
 * @param dev Client's device information which performed the request
 * @param info Information about the file to be deleted
 */
void delete_file(DEVICE dev, FileInfo& info);

/**
 * Sends the list of files from the client's directory.
 *
 * @param dev Client's device information which performed the request
 */
void list_files(DEVICE dev);

/**
 * Closes a connection to a specific client's device.
 * This information is also replicated to the replication servers.
 *
 * @param dev Client's device information which will be disconnected.
 */
void close_connection(DEVICE dev);

/**
 * Main function run by the replication server.
 * This function connects to the master server, spawns a thread to receive master requests,
 * and periodically checks if master is still alive.
 * If the master becomes offline, starts a new master election process.
 *
 * @param master_hostname Master server IP address
 * @param master_port Master server port
 * @return False when the replication server assumes the position of master server
 */
bool continue_as_slave(char* master_hostname, uint16_t master_port);

/**
 * Receives and handles master request.
 *
 * @param arg This argument is not handled at all (nullptr)
 * @return Returns a nullptr
 */
void* process_master_request(void* arg);

/**
 * Performs the setup of a new replication server.
 * This function also informs other replication servers of the newly connected one.
 *
 * @param handle Connection handle to the new replication server
 */
void setup_new_slave(ConnectionHandle* handle);

/**
 * Receives and acknowledge packets without further processing.
 * This function should be used just so that replication servers know that master is still up.
 *
 * @param _slave Replication server identification for which this function will send heartbeats.
 * @return Returns a nullptr
 */
void* slave_heartbeat(void* _slave);

/**
 * Sends the updated file information to all replication servers.
 *
 * @param file Updated file information
 * @param client Client's username associated with the file
 */
void update_file_on_slaves(FileInfo& file, std::string& client);
/**
 * Sends the update file information to a replication server.
 *
 * @param _args Structure BCKP_FILE_UPDT
 * @return Returns a nullptr
 */
void* slave_update_file(void* _args);

/**
 * Sends client information (login/logoff) to all replication servers.
 *
 * @param address Client's address
 * @param client Client's username
 * @param connected Indicator if client is logging in or off
 */
void update_client_on_slaves(std::string& address, std::string& client, bool connected);
/**
 * Sends client information (login/logoff) to a replication server.
 *
 * @param _args Structure BCKP_CLIENT_UPDT
 * @return Returns a nullptr
 */
void* slave_update_client(void* _args);

/**
 * Sends new replication server information to all other replication servers.
 *
 * @param address New replication server address
 * @param id New replication server identification
 */
void update_slave_on_slaves(std::string& address, int id);
/**
 * Sends new replication server information to another replication server.
 *
 * @param _args Structure BCKP_SLAVE_UPDT
 * @return Returns a nullptr
 */
void* slave_update_slave(void* _args);

/**
 * Procedure run by replication server when it is elected as new master server.
 * This function informs all connected client devices of the new master server address.
 */
void become_master();

#endif //DBOX_SERVER_H