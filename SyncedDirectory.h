//
// Created by gabriel on 5/28/18.
//

#ifndef DBOX_SYNCEDDIRECTORY_H
#define DBOX_SYNCEDDIRECTORY_H

#include <map>
#include <queue>
#include <memory>

#include <semaphore.h>

#include "FileInfo.h"
#include "MessageQueue.h"

//! Interval in seconds between synchronization of logical and physical directory
#define SYNC_TIMEOUT_S 5

/**
 * SYNCEDDIRECTORY
 *
 *  Logical representation of a physical directory
 */
class SyncedDirectory {

public:
    /**
     * Initialize an empty directory (not attached to physical directory)
     */
    SyncedDirectory();

    /**
     * Initializes a directory with the contents of its physical representation
     *
     * @param _path Path to the physical directory `/path/to/directory`
     */
    explicit SyncedDirectory(const std::string& _path);

    /**
     * Initializes a directory with the contents of its physical representation and
     * attach a MessageQueue which will receive all file updates
     *
     * @param _path Path to the physical directory
     * @param queue Queue which will store messages regarding file updates
     */
    SyncedDirectory(const std::string& _path, std::shared_ptr<MessageQueue> queue);

    /**
     * Returns the physical path of the directory
     *
     * @return /path/to/directory
     */
    const std::string& get_path();

    /**
     * Returns the file information stored in the directory
     *
     * @param name Name of the file
     * @return The associated file information
     */
    FileInfo& get_file_info(const std::string& name);

    /**
     * Returns a pointer to the store filed information
     *
     * @param name Name of the file
     * @return Pointer to the associated file information
     */
    FileInfo* get_file_info_ptr(const std::string& name);

    /**
     * Print the directory content with similar format to UNIX's `ls -l`
     */
    void print_directory();

    /**
     * Listen to changes in the physical directory and updates the logical one accordingly.
     * Any changes perceives are notified to the MessageQueue whenever set.
     */
    void start_local_sync();

    /**
     * Sets a file information on the directory.
     * If the file content is newer than the one stored logically, the physical directory is updated as well,
     * for instance, deletions and writes on physical disk.
     *
     * @param file New file information
     *
     * @return EXIT_SUCCESS if the file has been updated, EXIT_FAILURE otherwise
     */
    int set_file(FileInfo& file);

    /**
     * Writes the file content on the physical directory
     *
     * @param file File information
     */
    void write_file(FileInfo& file);

    /**
     * Deletes a file from the physical directory
     *
     * @param file File information
     */
    void delete_file(FileInfo& file);

    /**
     * Checks whether the file is contained in the logical directory
     *
     * @param filename Name of the file to be checked
     * @return Returns `true` if the file is located on the directory, `false` otherwise
     */
    bool has_file(std::string filename);

    /**
     * Serializes the directory in human readable format.
     * The string consists of comma separated file information in its serialized format.
     *
     * @return String representation of the directory
     */
    std::string serialize();

private:
    std::string path;
    std::map<std::string, FileInfo> files;
    std::shared_ptr<MessageQueue> queue;
    bool keep_syncing = true;

    sem_t access_files;

    /**
     * Reads the content of the physical directory and saves it on a hash map
     *
     * @param path Path to the physical directory
     * @param entries Hash map to save the entries
     */
    void read_directory(const std::string& path, std::map<std::string, FileInfo>& entries);

    /**
     * Add a message to the queue informing that a file has been deleted
     *
     * @param file Name of the deleted file
     */
    void push_deletion(std::string file);

    /**
     * Add a message to the queue informing that a file has been updated
     *
     * @param file Name of the updated file
     */
    void push_update(std::string file);
};


#endif //DBOX_SYNCEDDIRECTORY_H
