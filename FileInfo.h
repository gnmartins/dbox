//
// Created by Gabriel on 24/05/2018.
//

#ifndef DBOX_FILEINFO_H
#define DBOX_FILEINFO_H

#define MAXNAME 256

#include <string>
#include <memory>

/**
 * FILEINFO
 *
 *  Representation of a file, including its content.
 */
class FileInfo {

public:
    /**
     * Default constructor for an empty file.
     */
    FileInfo() = default;

    /**
     * Creates a new file information without its contents.
     *
     * @param name File's name
     * @param extension File's extension
     * @param last_modified File's last modified date (UNIX time format)
     * @param size File's size in bytes
     */
    FileInfo(const std::string& name, const std::string& extension, const std::string& last_modified, uint32_t size);

    /**
     * Creates a new file information without its contents.
     *
     * @param name File's name
     * @param extension File's extension
     * @param last_modified File's last modified date (UNIX time format)
     * @param size File's size in bytes
     * @param _data Pointer to the file's data
     */
    FileInfo(const std::string& name, const std::string& extension, const std::string& last_modified, uint32_t size,
             const char* _data);

    virtual ~FileInfo() = default;

    /**
     * Reads a file (including its contents) from a physical directory.
     *
     * @param path Path to the file `/path/to/file`
     * @return The file's information
     */
    static FileInfo read_file(const std::string& path) { return _read_file(path, true); }

    /**
     * Reads a file information (without contents) from a physical directory.
     *
     * @param path Path to the file `/path/to/file`
     * @return The file's information
     */
    static FileInfo read_file_info(const std::string& path) { return _read_file(path, false); }

    /**
     * Deserialize a file information.
     *
     * @param serialized Serialized string representing a file
     * @return The file's information
     */
    static FileInfo deserialize(std::string& serialized);
    /**
     * Deserialize a file information.
     *
     * @param serialized_char Pointer to the file's serialized representation
     * @param length Length of the file's serialized representation
     * @return The file's information
     */
    static FileInfo deserialize(char* serialized_char, uint32_t length);

    /**
     * Serializes a file in human readable format (with its contents).
     *
     * @return A string representing the file's information
     */
    std::string serialize() { return _serialize(false); }
    /**
     * Serializes a file in human readable format (without its contents).
     *
     * @return A string representing the file's information
     */
    std::string serialize_info() { return _serialize(true); }

    /**
     * Getter for file's name.
     *
     * @return The name of the file
     */
    std::string get_name();
    /**
     * Getter for file's extension.
     *
     * @return The extension of the file
     */
    std::string get_extension();
    /**
     * Returns the file's name in `name.extenstion` format.
     *
     * @return The full name of the file.
     */
    std::string get_filename();
    /**
     * Getter for file's last modified date.
     *
     * @return The last modified date of the file
     */
    std::string get_last_modified();
    /**
     * Returns the file's last modified date in human readable format.
     *
     * @return The last modified date of the file in format similar to UNIX's `ls -l`
     */
    std::string get_last_modified_date();
    /**
     * Getter for file's size.
     *
     * @return The size in bytes of the file
     */
    uint32_t get_size();
    /**
     * Getter for file's content.
     *
     * @return A byte array containing the file's content
     */
    std::shared_ptr<char> get_data();

    /**
     * Setter for file's last modified date.
     *
     * @param new_lm New last modified date
     */
    void set_last_modified(const std::string& new_lm);
    /**
     * Setter for file's size
     *
     * @param new_size New size in bytes
     */
    void set_size(uint32_t new_size);
    /**
     * Setter for file's content
     *
     * @param new_data Pointer to the new content's of the file
     */
    void set_data(std::shared_ptr<char> new_data);
    /**
     * Set the file as created (valid)
     */
    void set_as_created();
    /**
     * Set the file as deleted (invalid)
     */
    void set_as_deleted();

    /**
     * Checks whether the file is older than another one.
     * This check is performed by checking the last modified dates.
     *
     * @param other Other file's information
     * @return Returns `true` if this file has been modified before the other, `false` otherwise
     */
    bool is_older(FileInfo& other);
    /**
     * Checks whether the files is valid or invalid (has been deleted)
     *
     * @return Returns `true` if the file is valid, `false` otherwise
     */
    bool is_valid();

private:
    std::string name;
    std::string extension;
    std::string last_modified;
    uint32_t size;
    std::shared_ptr<char> data;

    bool validity = true;

    /**
     * Reads a file from a physical directory.
     *
     * @param path Path to the file `/path/to/file`
     * @param fetch_data Whether the file's content should be fetched as well
     * @return The file's information
     */
    static FileInfo _read_file(const std::string& path, bool fetch_data);

    /**
     * Serializes a file in human readable format
     *
     * @param ignore_data Whether the file's content should be serialized as well
     * @return A string representing the file's information
     */
    std::string _serialize(bool ignore_data);

};


#endif //DBOX_FILEINFO_H
