//
// Created by Gabriel on 23/05/2018.
//

#ifndef DBOX_PACKET_H
#define DBOX_PACKET_H

#include <string>
#include <memory>

/**
 * PACKET
 *
 *  Communication unit between peers
 */
class Packet {

public:
    /**
     * Creates a Packet without payload
     *
     * @param type Type of the packet
     * @param seqn Sequence number
     * @param total_size Total size of data being sent
     * @param length Length of the payload
     */
    Packet(uint16_t type, uint16_t seqn, uint32_t total_size, uint16_t length);

    /**
     * Creates a Packet with data stored in its payload
     *
     * @param type Type of the packet
     * @param seqn Sequence number
     * @param total_size Total size of data being sent
     * @param length Length of the payload
     * @param _payload Data content of the packet
     */
    Packet(uint16_t type, uint16_t seqn, uint32_t total_size, uint16_t length, const char* _payload);

    virtual ~Packet();

    /**
     * Creates an ACK packet based on the information of another packet
     *
     * @param packet Received packet
     * @return A packet that can be used to acknowledge a received packet
     */
    static Packet ack_packet(Packet& packet)
    { return Packet(Packet::ACK, packet.get_seqn(), packet.get_total_size(), 0); }

    /**
     * Syntactic sugar to create a data packet
     *
     * @param seqn Sequence number
     * @param total_size Total size of data being sent
     * @param length Length of the payload
     * @param payload Char array representing the content of the packet
     * @return A packet with type DATA
     */
    static Packet data_packet(uint16_t seqn, uint32_t total_size, uint16_t length, const char* payload)
    { return Packet(Packet::DATA, seqn, total_size, length, payload); }

    /**
     * Serializes the packet as a char array (string)
     *
     * @return A serialized version of the packet in readable form
     */
    std::string serialize();

    /**
     * Deserializes a packet and check its integrity
     *
     * @param serialized String representing a serialized packet
     * @return Data structure containing the information of the packet
     */
    static Packet deserialize(std::string& serialized);

    //! Maximum packet size in bytes (including payload)
    const static uint32_t MAX_PACKET_SIZE = 1250;

    //! Maximum payload length in bytes
    const static uint32_t MAX_PAYLOAD_LENGTH = 1000;

    /**
     * Packet types and their usages
     */
    enum {
        HELO,   //! Identify new peers
        GBYE,   //! End connections
        ALIV,   //! Check if peer is alive
        ACK,    //! Acknowledge received packet
        DATA,   //! Data transfer
        GET,    //! Request file
        PUT,    //! Upload file
        DEL,    //! Delete file
        LIST,   //! List files on peer
        OK,     //! Confirm request
        BCKP,   //! Replication-related messages
        VOTE,   //! Leader election
    };

    /**
     * Getter for Packet's type
     *
     * @return The type of the packet
     */
    uint16_t get_type();

    /**
     * Getter for Packet's sequence number
     *
     * @return The sequence number of the Packet
     */
    uint16_t get_seqn();

    /**
     * Getter for Packet's total size
     *
     * @return The total size of the data being transferred
     */
    uint32_t get_total_size();

    /**
     * Getter for Packet's payload length
     *
     * @return The payload length in bytes
     */
    uint16_t get_length();

    /**
     * Getter for Packet's payload
     *
     * @return A pointer to the payload's data
     */
    char* get_payload();

    /**
     * Sets packet as valid or invalid (corrupt)
     *
     * @param value Validity of the packet
     */
    void set_validity(bool value);

    /**
     * Checks packet integrity
     *
     * @return `true` if the packet is valid, `false` if corrupt
     */
    bool is_valid();

private:
    uint16_t type;
    uint16_t seqn;
    uint32_t total_size;
    uint16_t length;
    std::shared_ptr<char> payload;

    bool valid = true;

    /**
     * Calculates the CRC16 for a byte string (CRC-CCITT 0x1D0F)
     *
     * @param data Byte string
     * @param length Size of the byte string
     * @return The CRC16 for the data
     */
    uint16_t crc16(const unsigned char* data, uint16_t length);
};


#endif //DBOX_PACKET_H
