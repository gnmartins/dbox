//
// Created by Gabriel on 05/06/2018.
//
#include "ConnectionHandle.h"

#include <iostream>
#include <sstream>
#include <cstring>
#include <algorithm>

#include <netdb.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <time.h>

void print_socket_info(int sockfd)
{
    struct sockaddr_in tst;
    unsigned int addr_len = sizeof(struct sockaddr_in);

    getsockname(sockfd, (struct sockaddr*) &tst, &addr_len);
    std::cout << "Socket port: " << ntohs(tst.sin_port) << std::endl;
}

void print_addr_info(struct sockaddr_in addr)
{
    std::cout << "Addr port: " << ntohs(addr.sin_port) << std::endl;
}

ConnectionHandle::ConnectionHandle()
{
    sem_init(&semaphore_send, 0, 1);
}

ConnectionHandle::ConnectionHandle(char* hostname, uint16_t port, std::string username)
{
    struct hostent* server;

    server = gethostbyname(hostname);
    if (server == nullptr) {
        perror("[ConnectionHandle::ConnectionHandle] Error");
        exit(1);
    }

    sock_send = setup_send_socket(&addr_send, &port, (struct in_addr*) server->h_addr);
    sock_recv = sock_send;

    Packet helo_packet(Packet::HELO, 0, 0, (uint16_t) username.length(), username.c_str());
    if (send_packet(helo_packet) == EXIT_SUCCESS) {

        bool connection_established = false;
        while (!connection_established) {

            Packet packet = recv_packet();
            if (packet.get_type() == Packet::GBYE) {
                std::cout << "Connection refused by server." << std::endl;
                exit(1);
            }

            else if (packet.get_type() == Packet::HELO) {
                // server sent the confirmation packet with the socket it will be listening on
                sock_send = create_socket();
                addr_send = addr_recv;

                connection_established = true;
            }
        }
    }

    else {
        std::cout << "Server " << hostname << ":" << port << " could not be reached." << std::endl;
        connected = false;
    }

    sem_init(&semaphore_send, 0, 1);
}

ConnectionHandle::ConnectionHandle(uint16_t port)
{
    sock_recv = setup_recv_socket(&addr_recv, &port);

    bool connection_established = false;
    while (!connection_established) {

        // receiving connection request packet
        Packet helo_packet = recv_packet();
        if (helo_packet.get_type() != Packet::HELO) continue;

        // saves peer id
        peer_id = std::string(helo_packet.get_payload(), helo_packet.get_length());

        // address which client used to send should now be used by the server to send data
        addr_send = addr_recv;

        // creating a new socket to receive data from client
        close(sock_recv);
        sock_recv = setup_recv_socket(&addr_recv, nullptr);

        sock_send = sock_recv;

        Packet confirmation_packet = Packet(Packet::HELO, 0, 0, (uint16_t) peer_id.length(), peer_id.c_str());

        if (send_packet(confirmation_packet) == EXIT_SUCCESS) {
            sock_send = create_socket();

            connection_established = true;
        }
    }

    sem_init(&semaphore_send, 0, 1);

//    std::cout << "will send data to ";
//    print_addr_info(addr_send);
//
//    std::cout << "will recv data on ";
//    print_socket_info(sock_recv);
}

ConnectionHandle::~ConnectionHandle()
{
    shutdown(sock_recv, 2);
    close(sock_recv);
    shutdown(sock_send, 2);
    close(sock_send);
}

void ConnectionHandle::setup_sockets(std::string& peer, std::string& address)
{
    peer_id = peer;

    std::istringstream iss(address);
    std::string hostname, helper;

    std::getline(iss, hostname, ':');
    std::getline(iss, helper, ':');

    uint16_t port = (uint16_t) std::stoul(helper);

    struct hostent* server;
    server = gethostbyname(hostname.c_str());
    if (server == nullptr) {
        perror("[ConnectionHandle::setup_sockets] Error");
        exit(1);
    }

    sock_send = setup_send_socket(&addr_send, &port, (struct in_addr*) server->h_addr);
    sock_recv = setup_recv_socket(&addr_recv, nullptr);
}

std::string& ConnectionHandle::get_peer_id()
{
    return peer_id;
}

std::string ConnectionHandle::get_peer_address()
{
    char s_address[MAXNAME];
    inet_ntop(AF_INET, &(addr_send.sin_addr), s_address, INET_ADDRSTRLEN);

    std::ostringstream oss;
    oss << s_address << ":" << ntohs(addr_send.sin_port);

    return oss.str();
}

std::string ConnectionHandle::get_my_address()
{
    struct sockaddr_in tst;
    unsigned int addr_len = sizeof(struct sockaddr_in);

    getsockname(sock_recv, (struct sockaddr*) &tst, &addr_len);

    char s_address[MAXNAME];
    inet_ntop(AF_INET, &(tst.sin_addr), s_address, INET_ADDRSTRLEN);

    std::ostringstream oss;
    oss << s_address << ":" << ntohs(tst.sin_port);

    return oss.str();
}

Packet ConnectionHandle::recv_packet()
{
    long bytes_rcvd = 0;
    bool packet_rcvd = false;

    socklen_t addr_len = sizeof(addr_recv);

    char dgram[Packet::MAX_PACKET_SIZE];
    Packet packet(0, 0, 0, 0);

    while (!packet_rcvd) {
        bytes_rcvd = recvfrom(sock_recv, dgram, Packet::MAX_PACKET_SIZE, 0,
                              (struct sockaddr*) &addr_recv, &addr_len);

        if (bytes_rcvd < 0) continue;

        // checking packet integrity
        std::string serialized_packet(dgram, (unsigned long) bytes_rcvd);
        packet = Packet::deserialize(serialized_packet);
        if (!packet.is_valid()) continue;

        send_ack(packet, sock_recv, &addr_recv);
        packet_rcvd = true;
    }

    latest_message_rcvd = time(nullptr);

    return packet;
}

int ConnectionHandle::send_packet(Packet packet)
{
    std::string dgram = packet.serialize();

    int attempts = 0;
    long bytes_sent = 0;
    long bytes_rcvd = 0;
    bool ack_rcvd = false;

    struct sockaddr_in ack_addr{};
    unsigned int addr_len = sizeof(struct sockaddr_in);

    char ack_dgram[Packet::MAX_PACKET_SIZE];

    // setting timeout
    struct timeval tv{};
    tv.tv_sec = 0;
    tv.tv_usec = SEND_TIMEOUT_MS * 100;
    if (setsockopt(sock_send, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv)) < 0) {
        perror("Error");
        exit(1);
    }

    while (!ack_rcvd && attempts < MAX_SEND_ATTEMPTS) {
        attempts++;

        bytes_sent = sendto(sock_send, dgram.c_str(), dgram.size(), 0,
                            (const struct sockaddr*) &addr_send, sizeof(struct sockaddr_in));

        if (bytes_sent < dgram.size()) continue;

        // expecting ack
        bytes_rcvd = recvfrom(sock_send, ack_dgram, Packet::MAX_PACKET_SIZE, 0,
                              (struct sockaddr*) &ack_addr, &addr_len);

        if (bytes_rcvd < 0) continue;

        // checking packet integrity
        std::string serialized_packet(ack_dgram, (unsigned long) bytes_rcvd);
        Packet ack = Packet::deserialize(serialized_packet);
        if (!ack.is_valid()) continue;

        if (ack.get_type() == Packet::ACK && ack.get_seqn() == packet.get_seqn())
            ack_rcvd = true;
    }

    if (!ack_rcvd) {
//        std::cout << "Failed to send packet: too many failed attempts." << std::endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

std::shared_ptr<char> ConnectionHandle::recv_data(uint32_t total_size)
{
    std::shared_ptr<char> data(new char[total_size]);

    uint32_t total_bytes_rcvd = 0;
    uint16_t seqn = 0;

    long bytes_rcvd = 0;
    socklen_t addr_len = sizeof(struct sockaddr_in);

    char dgram[Packet::MAX_PACKET_SIZE];
    Packet packet(0, 0, 0, 0);

    while (total_bytes_rcvd < total_size) {

        bytes_rcvd = recvfrom(sock_recv, dgram, Packet::MAX_PACKET_SIZE, 0,
                              (struct sockaddr*) &addr_recv, &addr_len);

        if (bytes_rcvd < 0) continue;

        // checking packet integrity
        std::string serialized_packet(dgram, (unsigned long) bytes_rcvd);
        packet = Packet::deserialize(serialized_packet);
        if (!packet.is_valid()) continue;

        // if this is the expected packet
        if (packet.get_type() == Packet::DATA &&
            packet.get_total_size() == total_size) {

            // expected seqn or already received one
            if (packet.get_seqn() <= seqn) {
                send_ack(packet, sock_recv, &addr_recv);
            }

            // expected seqn => save data
            if (packet.get_seqn() == seqn) {
                // store data
                memcpy(data.get() + total_bytes_rcvd, packet.get_payload(), packet.get_length());

                // offset control variables
                total_bytes_rcvd += packet.get_length();
                seqn++;
            }
        }
    }

    return data;
}

int ConnectionHandle::send_data(std::shared_ptr<char> data, uint32_t total_size)
{
    uint32_t offset = 0;
    uint16_t payload_length = 0;

    for (uint16_t seqn = 0; offset < total_size; ++seqn) {

        // determine payload length based on remaining bytes to send
        if (offset + Packet::MAX_PAYLOAD_LENGTH > total_size)
            payload_length = (uint16_t) (total_size - offset);
        else
            payload_length = Packet::MAX_PAYLOAD_LENGTH;

        // create and send packet
        Packet data_packet = Packet::data_packet(seqn, total_size, payload_length, data.get()+offset);
        if (send_packet(data_packet) == EXIT_FAILURE) {
            return EXIT_FAILURE;
        }
        else {
            offset += payload_length;
        }

    }

    return EXIT_SUCCESS;
}

int ConnectionHandle::create_socket()
{
    int sockfd;
    if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) == -1) {
        perror("[ConnectionHandle::create_socket] Error");
        exit(1);
    }

    return sockfd;
}

int ConnectionHandle::setup_send_socket(struct sockaddr_in* sock_addr, uint16_t* port, struct in_addr* sin_addr)
{
    int sockfd = create_socket();

    if (sock_addr != nullptr && port != nullptr && sin_addr != nullptr) {
        sock_addr->sin_family = AF_INET;
        sock_addr->sin_port = htons(*port);
        sock_addr->sin_addr = *sin_addr;
        bzero(&(sock_addr->sin_zero), 8);
    }

    return sockfd;
}

int ConnectionHandle::setup_recv_socket(struct sockaddr_in* sock_addr, uint16_t* port)
{
    int sockfd = create_socket();

    sock_addr->sin_family = AF_INET;
    sock_addr->sin_port = (uint16_t) ((port == nullptr)? 0 : htons(*port));
    sock_addr->sin_addr.s_addr = INADDR_ANY;
    bzero(&(sock_addr->sin_zero), 8);

    if (bind(sockfd, (struct sockaddr*) sock_addr, sizeof(struct sockaddr)) < 0) {
        perror("[ConnectionHandle::setup_recv_socket] Error");
        exit(1);
    }

    return sockfd;
}

void ConnectionHandle::send_ack(Packet& data_packet, int sockfd, struct sockaddr_in* addr)
{
    Packet ack = Packet::ack_packet(data_packet);
    std::string dgram = ack.serialize();

    sendto(sockfd, dgram.c_str(), dgram.size(), 0,
           (const struct sockaddr*) addr, sizeof(struct sockaddr_in));
}

void ConnectionHandle::wait_send()
{
    sem_wait(&semaphore_send);
}

void ConnectionHandle::post_send()
{
    sem_post(&semaphore_send);
}

void ConnectionHandle::check_liveness()
{
    bool alive = true;

    while (alive) {
        // check if received any messages during an interval
        while (difftime(time(nullptr), latest_message_rcvd) < LIVENESS_CHECK_INTERVAL_S) {
            sleep(LIVENESS_CHECK_INTERVAL_S);
        }

        // sends packet to check liveness
        wait_send();
        if (send_packet(Packet(Packet::ALIV, 0, 0, 0)) == EXIT_SUCCESS) {
            latest_message_rcvd = time(nullptr);
        }
        else {
            alive = false;
        }
        post_send();
    }

    std::cout << "Peer is dead!" << std::endl;
}

void ConnectionHandle::update_send_addr(uint16_t port)
{
    char hostname[MAXNAME];
    inet_ntop(AF_INET, &(addr_recv.sin_addr), hostname, INET_ADDRSTRLEN);

    struct hostent* server;
    server = gethostbyname(hostname);
    if (server == nullptr) {
        perror("[ConnectionHandle::update_send_addr] Error");
        exit(1);
    }

    shutdown(sock_send, 2);
    close(sock_send);

    sock_send = setup_send_socket(&addr_send, &port, (struct in_addr*) server->h_addr);
}

bool ConnectionHandle::is_connected()
{
    return connected;
}

Packet ConnectionHandle::recv_packet_swapped()
{
    int sock_recv = sock_send;
    struct sockaddr_in addr_recv;

    long bytes_rcvd = 0;
    bool packet_rcvd = false;

    socklen_t addr_len = sizeof(struct sockaddr_in);

    char dgram[Packet::MAX_PACKET_SIZE];
    Packet packet(0, 0, 0, 0);

    while (!packet_rcvd) {
        bytes_rcvd = recvfrom(sock_recv, dgram, Packet::MAX_PACKET_SIZE, 0,
                              (struct sockaddr*) &addr_recv, &addr_len);

        if (bytes_rcvd < 0) continue;

        // checking packet integrity
        std::string serialized_packet(dgram, (unsigned long) bytes_rcvd);
        packet = Packet::deserialize(serialized_packet);
        if (!packet.is_valid()) continue;

        send_ack(packet, sock_recv, &addr_recv);
        packet_rcvd = true;
    }

    return packet;
}

int ConnectionHandle::send_packet_swapped(Packet packet)
{
    int sock_send = create_socket();
    struct sockaddr_in addr_send = addr_recv;

    std::string dgram = packet.serialize();

    int attempts = 0;
    long bytes_sent = 0;
    long bytes_rcvd = 0;
    bool ack_rcvd = false;

    struct sockaddr_in ack_addr{};
    unsigned int addr_len = sizeof(struct sockaddr_in);

    char ack_dgram[Packet::MAX_PACKET_SIZE];

    // setting timeout
    struct timeval tv{};
    tv.tv_sec = 0;
    tv.tv_usec = SEND_TIMEOUT_MS * 100;
    if (setsockopt(sock_send, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv)) < 0) {
        perror("Error");
        exit(1);
    }

    while (!ack_rcvd && attempts < MAX_SEND_ATTEMPTS) {
        attempts++;

        bytes_sent = sendto(sock_send, dgram.c_str(), dgram.size(), 0,
                            (const struct sockaddr*) &addr_send, sizeof(struct sockaddr_in));

        if (bytes_sent < dgram.size()) continue;

        // expecting ack
        bytes_rcvd = recvfrom(sock_send, ack_dgram, Packet::MAX_PACKET_SIZE, 0,
                              (struct sockaddr*) &ack_addr, &addr_len);

        if (bytes_rcvd < 0) continue;

        // checking packet integrity
        std::string serialized_packet(ack_dgram, (unsigned long) bytes_rcvd);
        Packet ack = Packet::deserialize(serialized_packet);
        if (!ack.is_valid()) continue;

        if (ack.get_type() == Packet::ACK && ack.get_seqn() == packet.get_seqn())
            ack_rcvd = true;
    }

    if (!ack_rcvd) {
//        std::cout << "Failed to send packet: too many failed attempts." << std::endl;
        shutdown(sock_send, 2);
        close(sock_send);
        return EXIT_FAILURE;
    }

    shutdown(sock_send, 2);
    close(sock_send);
    return EXIT_SUCCESS;
}

std::shared_ptr<char> ConnectionHandle::recv_data_swapped(uint32_t total_size)
{
    int sock_recv = sock_send;
    struct sockaddr_in addr_recv;

    std::shared_ptr<char> data(new char[total_size]);

    uint32_t total_bytes_rcvd = 0;
    uint16_t seqn = 0;

    long bytes_rcvd = 0;
    socklen_t addr_len = sizeof(struct sockaddr_in);

    char dgram[Packet::MAX_PACKET_SIZE];
    Packet packet(0, 0, 0, 0);

    while (total_bytes_rcvd < total_size) {

        bytes_rcvd = recvfrom(sock_recv, dgram, Packet::MAX_PACKET_SIZE, 0,
                              (struct sockaddr*) &addr_recv, &addr_len);

        if (bytes_rcvd < 0) continue;

        // checking packet integrity
        std::string serialized_packet(dgram, (unsigned long) bytes_rcvd);
        packet = Packet::deserialize(serialized_packet);
        if (!packet.is_valid()) continue;

        // if this is the expected packet
        if (packet.get_type() == Packet::DATA &&
            packet.get_seqn() == seqn &&
            packet.get_total_size() == total_size) {

            // store data
            memcpy(data.get()+total_bytes_rcvd, packet.get_payload(), packet.get_length());

            // offset control variables
            total_bytes_rcvd += packet.get_length();
            seqn++;

            // send ack
            send_ack(packet, sock_recv, &addr_recv);
        }
    }

    return data;
}

int ConnectionHandle::send_data_swapped(std::shared_ptr<char> data, uint32_t total_size)
{
    uint32_t offset = 0;
    uint16_t payload_length = 0;

    for (uint16_t seqn = 0; offset < total_size; ++seqn) {

        // determine payload length based on remaining bytes to send
        if (offset + Packet::MAX_PAYLOAD_LENGTH > total_size)
            payload_length = (uint16_t) (total_size - offset);
        else
            payload_length = Packet::MAX_PAYLOAD_LENGTH;

        // create and send packet
        Packet data_packet = Packet::data_packet(seqn, total_size, payload_length, data.get()+offset);
        if (send_packet_swapped(data_packet) == EXIT_FAILURE) {
            return EXIT_FAILURE;
        }
        else {
            offset += payload_length;
        }

    }

    return EXIT_SUCCESS;
}
