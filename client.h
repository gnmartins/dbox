//
// Created by Gabriel on 24/05/2018.
//

#ifndef DBOX_CLIENT_H
#define DBOX_CLIENT_H

#include <string>

/**
 * Performs the initial synchronization between server and client.
 * This function requests the download of the updated files on server-side, and updates the
 * message queue to send updated files on client-side
 */
void initial_sync();

/**
 * Receives and handles server requests.
 * This function is used to receive file updates from the server.
 *
 * @param args This argument is not handled
 * @return Returns a nullptr
 */
void* handle_server_request(void* args);

/**
 * Reads messages from the queue, and treat them accordingly.
 * This function is used to send file updates to the server.
 *
 * @param args This argument is not handled
 * @return Returns a nullptr
 */
void* file_sender(void* args);

/**
 * Loops on the directory's syncing function.
 * This function is used to check whether files have been changed in the physical directory.
 *
 * @param args This argument is not handled
 * @return Returns a nullptr
 */
void* local_sync(void* args);

/**
 * Handles user inputs on STDIN.
 */
void handle_user_input();

/**
 * Requests server to send a file.
 *
 * @param filename File's name
 */
void download_file(std::string& filename);

/**
 * Adds a file to the upload queue.
 *
 * @param filename Path to the file that should be uploaded to the server.
 */
void upload_file(std::string& filename);

/**
 * Adds a file to the deletion queue.
 *
 * @param filename File's name on sync_dir_<user>
 */
void delete_file(std::string& filename);

/**
 * List files contained on the clients sync_dir.
 */
void list_client();

/**
 * List files contained on the client's directory on server-side.
 */
void list_server();

/**
 * Close the connection to the server.
 */
void close_connection();

void wait_for_confirmation();

#endif //DBOX_CLIENT_H
