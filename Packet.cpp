//
// Created by Gabriel on 23/05/2018.
//

#include "Packet.h"

#include <cstring>
#include <iostream>
#include <sstream>

Packet::Packet(uint16_t type, uint16_t seqn, uint32_t total_size, uint16_t length)
        : type(type), seqn(seqn), total_size(total_size), length(length)
{
    payload.reset();
}

Packet::Packet(uint16_t type, uint16_t seqn, uint32_t total_size, uint16_t length, const char* _payload)
        : type(type), seqn(seqn), total_size(total_size), length(length)
{
    if (length > 0) {
        payload.reset(new char[length]);
        memcpy(payload.get(), _payload, length);
    }
    else {
        payload = nullptr;
    }
}

Packet::~Packet()
{

}

Packet Packet::deserialize(std::string& serialized)
{
    std::istringstream iss(serialized);

    std::string chk, hdr, tag, comma;
    uint16_t t, s, l;
    uint32_t ts;
    std::string p;

    iss >> chk >> hdr >> tag;
    iss >> t;

    iss >> comma >> tag;
    iss >> s;

    iss >> comma >> tag;
    iss >> ts;

    iss >> comma >> tag;
    iss >> l;

    if (l > 0) {
        iss >> comma >> tag;
        p = std::string(iss.str().substr(iss.tellg()));
    }

    Packet packet(t, s, ts, l, p.c_str()+1);

    // checksum
    std::istringstream iss2(packet.serialize());
    std::string chk2;
    iss2 >> chk2;
    if (chk != chk2) packet.set_validity(false);

    return packet;
}

std::string Packet::serialize()
{
    std::ostringstream oss;

    oss << "PKT ";
    oss << "type= " << type << ", ";
    oss << "seqn= " << seqn << ", ";
    oss << "total_size= " << total_size << ", ";
    oss << "length= " << length << ", ";

    if (payload != nullptr) {
        oss << "data= " << std::string(payload.get(), length);
    }
    else {
        oss << "data= ";
    }

    std::ostringstream pkt;
    pkt << crc16((unsigned char*) oss.str().c_str(), oss.str().size()) << " " << oss.str();

    return pkt.str();
}

uint16_t Packet::get_type()
{
    return type;
}

uint16_t Packet::get_seqn()
{
    return seqn;
}

uint32_t Packet::get_total_size()
{
    return total_size;
}

uint16_t Packet::get_length()
{
    return length;
}

char* Packet::get_payload()
{
    return payload.get();
}

void Packet::set_validity(bool value)
{
    valid = value;
}

bool Packet::is_valid()
{
    return valid;
}

uint16_t Packet::crc16(const unsigned char* data, uint16_t length)
{
    unsigned char x;
    uint16_t crc = 0x1d0f;

    while (length--) {
        x = crc >> 8 ^ *data++;
        x ^= x >> 4;
        crc = (crc << 8) ^ (x << 12) ^ (x << 5) ^ x;
    }

    return crc &= 0xffff;
}