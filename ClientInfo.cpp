//
// Created by Gabriel on 10/06/2018.
//

#include "ClientInfo.h"

#include <sstream>
#include <iostream>
#include <cstring>

ClientInfo::ClientInfo(std::string& userid)
    : userid(userid)
{
}

ClientInfo::ClientInfo(std::string& userid, char* path)
    : userid(userid)
{
    std::string dirpath(path, strlen(path));
    directory.reset(new SyncedDirectory(dirpath));
}

ClientInfo::ClientInfo(std::string& userid, ConnectionHandle* device)
    : userid(userid)
{
    connected_devices[0] = device;
}

ClientInfo::ClientInfo(std::string& userid, ConnectionHandle* device, char* path)
    : userid(userid)
{
    connected_devices[0] = device;

    std::string dirpath(path, strlen(path));
    directory.reset(new SyncedDirectory(dirpath));
}

ClientInfo ClientInfo::remote_client(uint16_t port)
{
    ConnectionHandle* device = new ConnectionHandle(port);
    return ClientInfo(device->get_peer_id(), device);
}

bool ClientInfo::is_logged_in()
{
    for (int i = 0; i < MAX_DEVICES; ++i)
        if (connected_devices[i] != nullptr)
            return true;

    return false;
}

ConnectionHandle* ClientInfo::get_device_by_id(int device_id)
{
    return connected_devices[device_id];
}

std::string ClientInfo::get_serialized_directory()
{
    return directory->serialize();
}

int ClientInfo::set_device(ConnectionHandle* device)
{
    int i;
    for (i = 0; i < MAX_DEVICES; ++i) {
        if (connected_devices[i] == nullptr)
            break;
    }

    if (i == MAX_DEVICES) return -1;

    connected_devices[i] = device;

    return i;
}

int ClientInfo::set_file(FileInfo& file)
{
    return directory->set_file(file);
}

std::string ClientInfo::get_directory_path()
{
    return directory->get_path();
}

FileInfo& ClientInfo::get_file(std::string& filename)
{
    return directory->get_file_info(filename);
}

bool ClientInfo::has_file(std::string& filename)
{
    return directory->has_file(filename);
}

void ClientInfo::remove_device(int device)
{
    connected_devices[device] = nullptr;
}

std::string ClientInfo::print_devices()
{
    std::ostringstream oss;

    int i = 0;
    while (i < MAX_DEVICES) {
        if (connected_devices[i] != nullptr) break;
        else i++;
    }

    oss << connected_devices[i]->get_peer_address();

    i++;
    while (i < MAX_DEVICES) {
        if (connected_devices[i] != nullptr) {
            oss << ", " << connected_devices[i]->get_peer_address();
        }
        i++;
    }

    return oss.str();
}