//
// Created by Gabriel on 28/05/2018.
//

#ifndef DBOX_MESSAGE_H
#define DBOX_MESSAGE_H

#include <string>

/**
 * MESSAGE
 *
 *  A simple object to encapsulate file change messages
 */
class Message {

public:
    /**
     * Creates a new message.
     *
     * @param type Message type
     * @param content Message content
     */
    Message(int type, std::string content);

    /**
     * Creates a new message copying the contents of another.
     *
     * @param x Message to copy information from
     */
    Message(const Message& x);
    virtual ~Message();

    /**
     * Message types for file updates
     */
    enum {
        MSG_DELE_FILE,  //! File has been deleted
        MSG_UPDT_FILE,  //! File has been updated
    };

    /**
     * Getter for the message type
     *
     * @return Returns the message's type
     */
    int get_type();

    /**
     * Getter for the message content
     *
     * @return Returns the message's content
     */
    std::string get_content();

    /**
     * Returns a string containing the message's information and type
     *
     * @return String representation of the message's information
     */
    std::string pretty_print();

private:
    int type;
    std::string content;

};


#endif //DBOX_MESSAGE_H
