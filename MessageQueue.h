//
// Created by Gabriel on 28/05/2018.
//

#ifndef DBOX_MESSAGEQUEUE_H
#define DBOX_MESSAGEQUEUE_H

#include <string>
#include <queue>

#include <semaphore.h>

#include "Message.h"

/**
 * MESSAGEQUEUE
 *
 *  Synchronized queue of messages in a producer/consumer style.
 */
class MessageQueue {

public:
    /**
     * Initializes a message queue.
     */
    MessageQueue();
    virtual ~MessageQueue();

    /**
     * Sets a new message to the queue.
     *
     * @param type Type of the message
     * @param content Content of the message
     */
    void set_message(int type, std::string content);

    /**
     * Gets a message from the queue.
     * The function caller remains locked until a message is available.
     *
     * @return The next message from the queue
     */
    Message get_message();

private:
    std::queue<Message> messages;

    sem_t semaphore_consumer;
    sem_t changing_queue;
};


#endif //DBOX_MESSAGEQUEUE_H
