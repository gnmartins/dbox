//
// Created by gabriel on 5/28/18.
//

#include "SyncedDirectory.h"

#include <iostream>
#include <sstream>
#include <fstream>
#include <cstdio>

#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>
#include <utime.h>

SyncedDirectory::SyncedDirectory()
{
    sem_init(&access_files, 0, 1);
}

SyncedDirectory::SyncedDirectory(const std::string& _path)
        : path(_path)
{
    sem_init(&access_files, 0, 1);

    if (path.back() == '/') path.pop_back();
    read_directory(path, files);
}

SyncedDirectory::SyncedDirectory(const std::string& _path, std::shared_ptr<MessageQueue> queue)
        : path(_path), queue(queue)
{
    sem_init(&access_files, 0, 1);

    if (path.back() == '/') path.pop_back();
    read_directory(path, files);
}

const std::string& SyncedDirectory::get_path()
{
    return path;
}

FileInfo& SyncedDirectory::get_file_info(const std::string& name)
{
    return files[name];
}

FileInfo* SyncedDirectory::get_file_info_ptr(const std::string& name)
{
    return &files[name];
}

int SyncedDirectory::set_file(FileInfo& file)
{
    sem_wait(&access_files);

    // this is a new file
    if (files.count(file.get_filename()) == 0) {
        files[file.get_filename()] = FileInfo(file.get_name(),
                                              file.get_extension(),
                                              file.get_last_modified(),
                                              file.get_size());
    }

    // this file is already in the directory
    else {
        FileInfo& old = get_file_info(file.get_filename());

        // file was updated => update object entry
        if (old.is_older(file)) {
            files[file.get_filename()].set_last_modified(file.get_last_modified());

            // if file was deleted
            if (!file.is_valid()) {
                files[file.get_filename()].set_as_deleted();
            }
            else {
                files[file.get_filename()].set_as_created();
                push_update(file.get_filename());
            }
        }

        // the file has an older timestamp => ignore
        else {
            sem_post(&access_files);
            return EXIT_FAILURE;
        }
    }

    // file is valid (was created/updated) => write to disk, set flag
    if (file.is_valid()) {
        files[file.get_filename()].set_as_created();
        write_file(file);
    }
    // file is invalid (was deleted) => delete from disk, set flag
    else {
        files[file.get_filename()].set_as_deleted();
        delete_file(file);
    }

    sem_post(&access_files);
    return EXIT_SUCCESS;
}

void SyncedDirectory::write_file(FileInfo& file)
{
    if (path.length() == 0) return;

    std::ostringstream oss;
    oss << path << "/" << file.get_filename();

    // setting data
    std::ofstream f;
    f.open(oss.str());
    f << std::string(file.get_data().get(), file.get_size());
    f.close();

    // setting mtime
    struct stat foo;
    struct utimbuf new_times;

    stat(oss.str().c_str(), &foo);
    long t = stol(file.get_last_modified());

    new_times.actime = foo.st_atime;    // keep atime unchanged
    new_times.modtime = t;              // set mtime to file time
    utime(oss.str().c_str(), &new_times);
}

void SyncedDirectory::delete_file(FileInfo& file)
{
    std::ostringstream oss;
    oss << path << "/" << file.get_filename();

    remove(oss.str().c_str());
}

bool SyncedDirectory::has_file(std::string filename)
{
    if (files.count(filename) > 0)
        return true;
    else
        return false;
}

void SyncedDirectory::print_directory()
{
    sem_wait(&access_files);

    for (auto& entry : files) {
        FileInfo& file_info = entry.second;
        if (!file_info.is_valid()) continue;
        std::cout << file_info.get_last_modified_date() << "\t";
        std::cout << file_info.get_size() << "\t";
        std::cout << file_info.get_filename() << std::endl;
    }

    sem_post(&access_files);
}

void SyncedDirectory::start_local_sync()
{
    keep_syncing = true;

    while (keep_syncing) {

        std::map<std::string, FileInfo> updated_dir;
        read_directory(path, updated_dir);

        // locking access to hash
        sem_wait(&access_files);

        // updated files
        // new_entry -> information just read from directory
        // old_entry -> information previously stored
        for (auto& new_entry : updated_dir) {
            // file existed before this synchronization
            if (files.count(new_entry.first) > 0) {
                FileInfo old_ = get_file_info(new_entry.first);
                FileInfo new_ = new_entry.second;

                // checking if file has been updated
                if (old_.is_older(new_)) {
                    //std::cout << "Updated file: " << new_entry.first << std::endl;

                    // update information (only last modified date have changed -- data is not relevant here)
                    files[new_entry.first].set_last_modified(new_.get_last_modified());
                    files[new_entry.first].set_as_created();
                    push_update(new_entry.first);
                }
            }

            // new file
            else {
                //std::cout << "New file: " << new_entry.first << std::endl;

                files[new_entry.first] = new_entry.second;
                push_update(new_entry.first);
            }
        }

        // deleted files
        for (auto& old_entry : files) {
            if (old_entry.second.is_valid() && updated_dir.count(old_entry.first) <= 0) {
                //std::cout << "File was deleted: " << old_entry.first << std::endl;

                std::ostringstream oss;
                oss << time(nullptr);
                files[old_entry.first].set_last_modified(oss.str());
                files[old_entry.first].set_as_deleted();
                push_deletion(old_entry.first);
            }
        }

        // releasing lock
        sem_post(&access_files);

        sleep(SYNC_TIMEOUT_S);
    }
}

void SyncedDirectory::read_directory(const std::string& path, std::map<std::string, FileInfo>& entries)
{
    DIR* dir;
    struct dirent* ent;
    struct stat fs;

    if ((dir = opendir(path.c_str())) != nullptr) {
        while ((ent = readdir(dir)) != nullptr) {

            std::ostringstream x;
            x << path << "/" << ent->d_name;

            // ignoring lstat errors
            if (lstat(x.str().c_str(), &fs) != 0) {
                perror(x.str().c_str());
                continue;
            }

            // ignoring directories
            if (S_ISDIR(fs.st_mode)) {
                continue;
            }

            // reading file information
            entries[ent->d_name] = FileInfo::read_file_info(x.str());
        }
    }

    else {
        perror("SyncedDirectory");
        exit(EXIT_FAILURE);
    }

    closedir(dir);
}

void SyncedDirectory::push_deletion(std::string file)
{
    if (queue != nullptr)
        queue->set_message(Message::MSG_DELE_FILE, file);
}

void SyncedDirectory::push_update(std::string file)
{
    std::ostringstream oss;
    oss << path << "/" << file;
    if (queue != nullptr)
        queue->set_message(Message::MSG_UPDT_FILE, oss.str());
}

std::string SyncedDirectory::serialize()
{
    std::ostringstream oss;
    bool first = true;

    for (auto& entry : files) {
        FileInfo& file = entry.second;

        if (first) {
            oss << file.serialize_info();
            first = false;
        }
        else {
            oss << "|" << file.serialize_info();
        }
    }

    return oss.str();
}