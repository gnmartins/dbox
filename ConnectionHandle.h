//
// Created by Gabriel on 05/06/2018.
//

#ifndef DBOX_CONNECTIONHANDLE_H
#define DBOX_CONNECTIONHANDLE_H

#include <cstdint>
#include <memory>

#include <sys/types.h>
#include <netinet/in.h>
#include <semaphore.h>

#include "Packet.h"
#include "FileInfo.h"

/**
 * CONNECTIONHANDLE
 *
 *  Representation of a connection between peers.
 */
class ConnectionHandle {

public:
    /**
     * Creates an empty connection handle (no peer information).
     */
    ConnectionHandle();

    /**
     * Creates a connection handle to a specific peer.
     *
     * @param hostname Peer's hostname (IPv4 address)
     * @param port Peer's port
     * @param username Self identification
     */
    ConnectionHandle(char* hostname, uint16_t port, std::string username);

    /**
     * Waits for a peer's connection.
     * This works similar to TCP's listen and accept, creating a new socket to receive messages.
     *
     * @param port Port to listen for new peers.
     */
    explicit ConnectionHandle(uint16_t port);

    virtual ~ConnectionHandle();

    /**
     * Syntactic sugar to connect to a server.
     *
     * @param hostname Server's hostname
     * @param port Server's port
     * @param username Self (client) identification
     * @return A connection handle to the server
     */
    static ConnectionHandle connect(char* hostname, uint16_t port, std::string username)
        { return ConnectionHandle(hostname, port, username); }

    /**
     * Syntactic sugar to receive clients.
     *
     * @param port Port to listen for new clients.
     * @return A connection handle to the client.
     */
    static ConnectionHandle listen(uint16_t port)
        { return ConnectionHandle(port); }

    /**
     * Setup sockets to send and receive data from a peer.
     * This function does not create a connection by handshaking.
     *
     * @param peer Peer's identification
     * @param address Peer's address in format `<address>:<port>`
     */
    void setup_sockets(std::string& peer, std::string& address);

    //! Maximum attempts to send a packet without acknowledgement
    const static int MAX_SEND_ATTEMPTS = 10;
    //! Timeout between packet send attempts
    const static int SEND_TIMEOUT_MS = 150;
    //! Interval to wait between heartbeat checks
    const static int LIVENESS_CHECK_INTERVAL_S = 10;

    /**
     * Getter for peer's identification
     *
     * @return The peer's username
     */
    std::string& get_peer_id();
    /**
     * Getter for peer's address
     *
     * @return The peer's address in format `<address>:<port>`
     */
    std::string get_peer_address();
    /**
     * Getter for self receiving address
     *
     * @return Self address in format `<address>:<port>`
     */
    std::string get_my_address();

    /**
     * Receives a packet from the peer.
     *
     * @return The received packet
     */
    Packet recv_packet();

    /**
     * Sends a packet to the peer.
     *
     * @param packet Packet to be sent
     * @return EXIT_SUCCESS if the packet has been acknowledged, EXIT_FAILURE otherwise
     */
    int send_packet(Packet packet);

    /**
     * Receives bytes from the peer.
     *
     * @param total_size Total amount of bytes to be received
     * @return A pointer to the data received
     */
    std::shared_ptr<char> recv_data(uint32_t total_size);

    /**
     * Sends bytes to the peer.
     *
     * @param data A pointer to the data that should be sent
     * @param total_size Total size of the data
     * @return EXIT_SUCCESS if data has been acknowledged, EXIT_FAILURE otherwise
     */
    int send_data(std::shared_ptr<char> data, uint32_t total_size);

    /**
     * Receives a packet from the peer, but on the socket used normally to send.
     *
     * @return The received packet.
     */
    Packet recv_packet_swapped();

    /**
     * Sends a packet to the peer, but to the destination from which the last packet was received.
     *
     * @param packet Packet to be sent.
     * @return EXIT_SUCCESS if the packet has been acknowledged, EXIT_FAILURE otherwise
     */
    int send_packet_swapped(Packet packet);

    /**
     * Receives bytes from the peer, but on the socket used normally to send.
     *
     * @param total_size Total amount of bytes to be received
     * @return A pointer to the data received
     */
    std::shared_ptr<char> recv_data_swapped(uint32_t total_size);

    /**
     * Sends bytes to the peer, but to the destination from which the last packet was received.
     *
     * @param data A pointer to the data that should be sent
     * @param total_size Total size of the data
     * @return EXIT_SUCCESS if data has been acknowledged, EXIT_FAILURE otherwise
     */
    int send_data_swapped(std::shared_ptr<char> data, uint32_t total_size);

    /**
     * Wait on semaphore to use the send socket.
     */
    void wait_send();
    /**
     * Signal the semaphore of the send socket.
     */
    void post_send();

    /**
     * Loops checking for peer's heartbeat.
     */
    void check_liveness();

    /**
     * Updates the address to send data.
     * The IP address is extracted from the address which sent the latest received packet.
     *
     * @param port New port to send data to
     */
    void update_send_addr(uint16_t port);

    /**
     * Checks whether the peer is connected.
     *
     * @return Returns `true` if the peer is still connected, `false` otherwise
     */
    bool is_connected();

private:
    int sock_recv;
    struct sockaddr_in addr_recv;

    int sock_send;
    struct sockaddr_in addr_send;
    sem_t semaphore_send;

    std::string peer_id;

    time_t latest_message_rcvd = time(nullptr);
    pthread_t thr_keep_alive;

    bool connected = true;

    /**
     * Creates a new socket.
     *
     * @return Socket file descriptor
     */
    int create_socket();

    /**
     * Setups a new socket and the address to send data.
     *
     * @param sock_addr Pointer to address information
     * @param port Port to send data to
     * @param sin_addr Peer's IP address
     * @return Socket file descriptor
     */
    int setup_send_socket(struct sockaddr_in* sock_addr, uint16_t* port, struct in_addr* sin_addr);

    /**
     * Setups a new socket to receive data.
     *
     * @param sock_addr Pointer to the address information
     * @param port Port to receive data on
     * @return Socket file descriptor
     */
    int setup_recv_socket(struct sockaddr_in* sock_addr, uint16_t* port);

    /**
     * Sends an ACK packet.
     *
     * @param data_packet Packet that should be acknowledged
     * @param sockfd Socket to send ack on
     * @param addr Address to send ack to
     */
    void send_ack(Packet& data_packet, int sockfd, struct sockaddr_in* addr);
};


#endif //DBOX_CONNECTIONHANDLE_H
